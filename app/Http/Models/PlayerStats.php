<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerStats extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'player_stats';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['server_id', 'username', 'user_id', 'mins_online', 'last_online'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id', 'server_id', 'user_id'];

    public $timestamps = false;
}
