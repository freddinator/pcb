<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Ban extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banlist';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'server_id',
        'is_active',
        'is_global_ban',
        'staff_id',
        'staff_name',
        'staff_uuid',
        'player_name',
        'player_uuid',
        'ban_reason',
        'ban_date',
        'unban_date'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];

    public $timestamps = false;
}
