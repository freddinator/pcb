<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ForumLike extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forum_likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'parent_id', 'parent_type', 'created_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id', 'parent_type'];

    public $timestamps = false;

    public function Parent()
    {
        return $this->morphTo('parent');
    }
}
