<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ForumPoll extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forum_polls';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['thread_id', 'question', 'locks_at', 'max_choice', 'vote_requires_post', 'anonymous_votes', 'votes_visible'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function Choices()
    {
        return $this->hasMany('App\Http\Models\ForumPollChoice', 'poll_id');
    }
}
