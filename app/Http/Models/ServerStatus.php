<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ServerStatus extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'server_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['server_id', 'is_online', 'current_players', 'max_players', 'players', 'date'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];

    public $timestamps = false;
}
