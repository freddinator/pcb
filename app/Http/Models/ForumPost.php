<?php

namespace App\Http\Models;

use App\Http\Traits\ThreadModel;
use Illuminate\Database\Eloquent\Model;

class ForumPost extends Model
{
    use ThreadModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forum_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['parent_id', 'parent_type', 'user_id', 'message'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
