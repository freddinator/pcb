<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ForumPollChoice extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forum_poll_choices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['poll_id', 'choice'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    public function Votes()
    {
        return $this->hasMany('App\Http\Models\ForumPollVote', 'choice_id');
    }
}
