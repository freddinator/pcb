<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ForumCategory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forum_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'order'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    public function Boards()
    {
        return $this->hasMany('App\Http\Models\ForumBoard', 'cat_id', 'id');
    }
}
