<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ForumBoard extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'forum_boards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'cat_id', 'thread_model', 'total_threads', 'total_posts', 'last_post_id', 'last_thread_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;

    public function Category()
    {
        return $this->hasOne('App\Http\Models\ForumCat', 'cat_id');
    }

    public function ParentBoard()
    {
        return $this->belongsTo('App\Http\Models\ForumBoard', 'parent_id');
    }

    public function ChildBoards()
    {
        return $this->hasMany('App\Http\Models\ForumBoard', 'parent_id');
    }


    public function LatestThread()
    {
        return $this->hasOne('App\Http\Models\ForumThread', 'id', 'last_thread_id')->with('Root');
    }

    public function LatestPost()
    {
        return $this->hasOne('App\Http\Models\ForumPost', 'id', 'last_post_id');
    }
}
