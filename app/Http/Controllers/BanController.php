<?php

namespace App\Http\Controllers;

use App\Http\API\AuthAPI;
use App\Http\API\BanAPI;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Response;

class BanController extends Controller
{
    use ThrottlesLogins;

    private $banAPI;
    public function __construct(BanAPI $banAPI)
    {
        $this->banAPI = $banAPI;
    }

    public function GetList()
    {
        return $this->banAPI->GetList();
    }
}
