<?php

namespace App\Http\Controllers;

use App\Http\API\RegisterAPI;
use Response;

class RegisterController extends Controller
{
    private $registerAPI;
    public function __construct(RegisterAPI $registerAPI)
    {
        $this->registerAPI = $registerAPI;
    }

    public function CreateAccount()
    {
        return Response::json( $this->registerAPI->CreateAccount() );
    }

    public function CheckUsername()
    {
        return Response::json( $this->registerAPI->CheckUsername() );
    }
}
