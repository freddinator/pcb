<?php

namespace App\Http\Controllers;

use App\Http\API\ForumPostAPI;
use Response;

class ForumPostController extends Controller
{
    private $forumAPI;
    public function __construct(ForumPostAPI $forumAPI)
    {
        $this->forumAPI = $forumAPI;
    }

    public function Create()
    {
        return $this->forumAPI->CreatePost();
    }

    public function GetLatest()
    {
        return $this->forumAPI->GetLatest();
    }
}
