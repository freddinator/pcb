<?php

namespace App\Http\Controllers;

use App\Http\API\ForumPollAPI;
use App\Http\API\UserAPI;
use App\Http\Models\User;
use Response;

class UserController extends Controller
{
    private $forumAPI;
    public function __construct(UserAPI $forumAPI)
    {
        $this->forumAPI = $forumAPI;
    }

    public function GetRoles()
    {
        return $this->forumAPI->GetRoles();
    }

    public function GetList()
    {
        return $this->forumAPI->GetList();
    }
}
