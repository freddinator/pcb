<?php

namespace App\Http\Controllers;

use App\Http\API\MaintenanceAPI;
use Response;

class MaintenanceController extends Controller
{
    private $forumAPI;
    public function __construct(MaintenanceAPI $forumAPI)
    {
        $this->forumAPI = $forumAPI;
    }

    public function RecountBoardStats()
    {
        return $this->forumAPI->RecountBoardStats();
    }

    public function ClearRead()
    {
        return $this->forumAPI->ClearRead();
    }
}
