<?php
namespace App\Http\Processors\Servers;

use xPaw\MinecraftQuery;
use xPaw\MinecraftQueryException;

use Log;

class QueryMinecraft implements IServerQuery
{
    public function GetStatus($ip, $port)
    {
        $Query = new MinecraftQuery();

        try
        {
            $Query->Connect($ip, $port);

            $data = $Query->GetInfo();
            $players = $Query->GetPlayers();

            $status['is_online']        = true;
            $status['players']          = implode(",", $players);
            $status['current_players']  = $data['Players'];
            $status['max_players']      = $data['MaxPlayers'];
        }
        catch( MinecraftQueryException $e )
        {
            $status['is_online']        = false;
            Log::notice("Minecraft query exception: " . $e->getMessage());
        }

        return $status;
    }
}