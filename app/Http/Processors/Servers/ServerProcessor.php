<?php
namespace App\Http\Processors\Servers;

use App\Http\Models\Server;
use App\Http\Models\ServerStatus;
use App\Http\Models\PlayerStats;
use Carbon\Carbon;
use Log;
use Cache;

/**
 * Class ServerProcessor
 * @package App\Http\Processors\Servers
 *
 * An adapter for pinging/querying the status of different game servers
 */
class ServerProcessor
{
    private $QUERY_RETRY_COUNT = 1;         // 0 = OFF | 1 = one retry attempt

    /**
     * Looks up a server by id then queries it using the appropriate handler
     *
     * @param $id
     * @return static
     */
    public function GetStatus($id)
    {
        $server = Server::find($id);

        $query = null;
        switch($server->type)
        {
            case 'minecraft':
                $query = new QueryMinecraft();
                break;
            case 'teamspeak':
                $query = new QueryTeamSpeak();
                break;
            default:
                Log::error('Unknown server type passed for querying: ' . $server->type);
                return null;
        }

        $status = $query->GetStatus($server->ip, $server->port);

        // if the server is offline, retry to ensure it wasn't a lag spike
        if(!$status['is_online'])
        {
            for($i = 0; $i < $this->QUERY_RETRY_COUNT; $i++)
            {
                $status = $query->GetStatus($server->ip, $server->port);
                if($status['is_online'])
                    break;
            }
        }

        $status['server_id']    = $server->id;
        $status['date']         = Carbon::now();

        // if offline, get last known MaxPlayers count if it exists
        if(!$status['is_online'])
        {
            $last_status = ServerStatus::where('server_id', '=', $server->id)->orderBy('date', 'desc')->first();
            if($last_status != null)
                $status['max_players']  = $last_status->max_players;
        }

        // flush the cache
        $row = ServerStatus::create($status);
        $key = 'server_status_' . $server->id;
        Cache::forget($key);
        Cache::forever($key, $row);

        return $row;
    }

}