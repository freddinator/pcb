<?php
namespace App\Http\Processors\Stats;

use App\Http\Models\Server;
use App\Http\Models\ServerStatus;
use App\Http\Models\PlayerStats;
use App\Http\Processors\Stats\Server\ParserPlayerListToday;
use App\Http\Processors\Stats\Server\ParserPlaytime;
use App\Http\Processors\Stats\Server\ParserUptime;
use App\Http\Processors\Stats\Server\ParserWeeklyOnline;
use Cache;

class ServerStats2 {

    /*
     * The list of queries to perform on each status
     */
    private $query_chain = array();

    private $cache_key = 'server_stats-parsed-id';

    private $cache;

    public function __construct()
    {
        // default stat queries to perform
        $this->query_chain = [
            new ParserPlaytime(),
            new ParserUptime(),
            new ParserPlayerListToday(),
            new ParserWeeklyOnline()
        ];
    }

    /**
     * Takes the latest unparsed statuses for the specified server (or each server if none given)
     * and parses it.
     *
     * @param null $server_id   (Optional) The server to parse
     */
    public function ParseLatest($server_id = null)
    {
        // TODO: this cache is not going to work with multiple servers...
        $this->cache = Cache::get($this->cache_key);
        if($this->cache == null)
            $this->cache = 0;

        if($server_id == null)
        {
            // parse each server
            $servers = Server::all();
            foreach($servers as $server)
            {
                $this->ParseLatest($server->id);
            }
            return;
        }

        $statuses = ServerStatus::orderBy('date', 'asc')
            ->where('server_id', $server_id)
            ->where('id', '>', $this->cache)
            ->get();

        foreach($statuses as $status)
        {
            $this->Parse($status);
        }

        $this->FinishParse();

        Cache::forever($this->cache_key, $this->cache);
    }

    /**
     * Takes every status in the database and parses them.
     * Extremely resource intensive - should only be run at website launch!
     *
     * @param Int $server_id    (Optional) The server to parse
     */
    public function ParseFromBeginning($server_id = null)
    {
        Cache::forget($this->cache_key);
        $this->FirstRun();

        $statuses = null;
        if($server_id == null)
        {
            // parse all servers (one at a time)
            foreach(Servers::all() as $server)
            {
                $this->ParseFromBeginning($server->id);
            }
            return;
        }

        // parse only the specified server
        PlayerStats::where('server_id', $server_id)->delete();
        $statuses = ServerStatus::orderBy('date', 'asc')
            ->where('server_id', $server_id)
            ->get();

        foreach($statuses as $status)
        {
            $this->Parse($status);
        }

        $this->FinishParse();


        Cache::forever($this->cache_key, $this->cache);
    }

    /**
     * Overrides the default query command chain to perform on every status
     *
     * @param $query_chain
     */
    public function SetQueryChain($query_chain)
    {
        $this->query_chain = $query_chain;
    }

    /**
     * Parses a single given status
     *
     * @param $status       The status to parse
     */
    private function Parse($status)
    {
        // check if we've already parsed an ID equal or higher than this
        if($this->cache == null)
        {
            $this->cache = Cache::get($this->cache_key);
        }

        if($this->cache != null)
        {
            if($status->id <= $this->cache)
                return;
        }

        foreach($this->query_chain as $query)
            $query->Parse($status);

        $this->cache = $status->id;
    }

    /**
     * Invokes the OnComplete() event on every query in the current chain
     */
    private function FinishParse()
    {
        foreach($this->query_chain as $query)
            $query->OnComplete();
    }

    /**
     * Invokes the OnFirstRun() event on every query in the current chain
     */
    private function FirstRun()
    {
        foreach($this->query_chain as $query)
            $query->OnFirstRun();
    }

    /**
     * Gets the stats created by each parser
     *
     * @param null $server_id
     * @param null $query_chain
     * @return array
     */
    public function Get($server_id = null, $query_chain = null)
    {
        if($query_chain == null)
            $query_chain = $this->query_chain;

        $stats = array();
        foreach($query_chain as $query)
        {
            $data = $query->GetStats();
            if(!$data[0])
                continue;

            if($server_id != null)
            {
                $all_data = $data[1];
                if(array_key_exists($server_id, $all_data))
                {
                    $data[1] = $data[1][$server_id];
                }
            }

            $stats[$data[0]] = $data[1];
        }

        return $stats;
    }
}