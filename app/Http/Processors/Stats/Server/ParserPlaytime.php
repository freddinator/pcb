<?php
namespace App\Http\Processors\Stats\Server;

use App\Http\Models\PlayerStats;
use Cache;
use Carbon\Carbon;

class ParserPlaytime extends AbstractServerStatParser {

    /*
     * The max minute threshold between two statuses before a player's playtime is no longer added together
     * (eg. threshold = 20; gap of statuses = 30 minutes; considered as two different play sessions)
     */
    private $playtime_gap_threshold = 20;

    protected $cache_key = 'server_stats-parser-playtime';
    protected $output_name = 'players_playtime';
    protected $output = false;

    private $last_status;
    private $current_status;

    private $players_to_update = array();

    /**
     * Updates (or inserts if new) the playtime of players based on the current and previous server status
     *
     * @param $status
     * @return mixed|void
     */
    public function Parse($status)
    {
        $this->current_status = $status;
        $server_id = $status->server_id;

        // retrieve cache data if it exists - otherwise build it and then exit
        if($this->last_status == null)
        {
            $is_entry = $this->GetCacheEntry($server_id, $this->last_status, function() use($status) {
                return $status;
            });

            if(!$is_entry)
                return;
        }

        $last_time    = new Carbon($this->last_status->date);
        $current_time = new Carbon($status->date);
        $time_diff = $current_time->diffInMinutes($last_time);

        // has the time gap between statuses exceeded the threshold?
        if($time_diff > $this->playtime_gap_threshold)
        {
            $this->last_status = $status;
            return;
        }

        // check for player differences
        $last_players    = explode(",", $this->last_status->players);
        $current_players = explode(",", $status->players);
        $player_list = array_merge($last_players, $current_players);

        foreach($player_list as $player)
        {
            if( in_array($player, $last_players) && !in_array($player, $current_players) )
            {
                // player has disconnected since last status
            }
            else
            {
                // otherwise add the player to the temp list to be bulk inserted/updated @ OnComplete()
                if(!array_key_exists($player, $this->players_to_update))
                {
                    $this->players_to_update[$player] = [
                        'mins_online'   => $time_diff,
                        'last_seen'     => $current_time
                    ];
                }
                else
                {
                    $this->players_to_update[$player]['mins_online'] += $time_diff;
                }
            }
        }

        $this->last_status = $status;
    }

    /**
     * Bulk insert/update all players that have appeared in any of the parsed statuses
     *
     * @return mixed|void
     */
    public function OnComplete()
    {
        // cache the current status for use next time
        if($this->current_status != null)
        {
            $cache = Cache::get($this->cache_key);
            $cache[$this->current_status->server_id] = $this->current_status;
            Cache::forever($this->cache_key, $cache);
        }

        // determine which players already exist in storage
        $players = array_keys($this->players_to_update);
        $existing_players = PlayerStats::whereIn('username', $players)->get();

        // update existing players
        $updated_players = array();
        foreach($existing_players as $player)
        {
            $player->mins_online += $this->players_to_update[$player->username]['mins_online'];
            $player->last_online  = $this->players_to_update[$player->username]['last_seen'];
            $player->save();

            array_push($updated_players, $player->username);
        }

        // insert new players
        $new_players = array_diff($players, $updated_players);
        $data = array();
        foreach($new_players as $player)
        {
            $row = array();
            $row['server_id'] = $this->last_status->server_id;
            $row['username'] = $player;
            $row['mins_online'] = $this->players_to_update[$player]['mins_online'];
            $row['last_online'] = $this->players_to_update[$player]['last_seen'];

            array_push($data, $row);
        }

        PlayerStats::insert($data);
    }


    public function OnFirstRun()
    {
        Cache::forget($this->cache_key);
    }
}