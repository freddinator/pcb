<?php
namespace App\Http\Processors\RemoteSync;

use App\Http\Models\EconomyTransaction;
use Carbon\Carbon;

class RemoteSyncerEconomy extends AbstractRemoteSyncer {

    public $key = "economy_transactions";
    public $remote_model = "\App\Http\Models\Remote\CSTransaction";
    public $local_model = "\App\Http\Models\EconomyTransaction";
    public $id_column = "Id";
    public $process_limit = 5000;

    /**
     * Logic to apply to every row in the remote table.
     * Mutation logic should go here.
     *
     * @param $row
     * @return array    A single associative array to be imported locally as a row
     */
    public function OnParseRow($row)
    {
        $time = Carbon::createFromTimestamp($row->Time);

        //TODO: check for user id existence

        $item = explode(":", $row->ItemId);
        $item_id = $item[0];
        $item_sub_id = count($item) > 1 ? $item[1] : null;

        return [
            'shop_uuid'         => $row->ShopOwnerId,
            'customer_uuid'     => $row->CustomerId,
            'item'              => $item_id,
            'item_sub_id'       => $item_sub_id,
            'transaction_type'  => $row->Mode,
            'amount'            => $row->Amount,
            'quantity'          => $row->Quantity,
            'time'              => $time
        ];
    }

    /**
     * Invoked when either all rows are processed or the limit is reached.
     * Local db import logic should go here.
     *
     * @param array $rows An entire array of associative arrays. Each row corresponds to a single row to import
     */
    public function OnBulkImport($rows)
    {
        EconomyTransaction::insert($rows);
    }
}