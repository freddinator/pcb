<?php
namespace App\Http\Helpers;

/**
 * Represents a span of time (eg. 1 hour and 30 seconds)
 *
 * Class Timespan
 * @package App\Http\Helpers
 */
class Timespan {

    private $span;  // total time span (in seconds)

    private $seconds;
    private $minutes;
    private $hours;
    private $days;

    public function __construct($seconds = 0, $minutes = 0, $hours = 0, $days = 0)
    {
        $this->span = $seconds + ($minutes * 60) + ($hours * 3600) + (86400 * $days);
        $this->UpdateSpan();
    }

    /**
     * Adds the specified number of seconds to the total timespan
     *
     * @param $seconds
     */
    public function AddSeconds($seconds)
    {
        $this->span += $seconds;
        $this->UpdateSpan();
    }

    public function AddMinutes($minutes)
    {
        $this->AddSeconds($minutes * 60);
    }

    public function AddHours($hours)
    {
        $this->AddSeconds($hours * 3600);
    }

    public function AddDays($days)
    {
        $this->AddSeconds($days * 86400);
    }

    /**
     * Converts the stored span (in seconds) to [seconds, minutes, hours & days] and stores it
     */
    private function UpdateSpan()
    {
        $this->seconds = $this->span;

        $this->minutes = 0;
        if($this->seconds >= 60)
        {
            $this->minutes = floor($this->seconds / 60);
            $this->seconds -= $this->minutes * 60;
        }

        $this->hours = 0;
        if($this->minutes >= 60)
        {
            $this->hours = floor($this->minutes / 60);
            $this->minutes -= $this->hours * 60;
        }

        $this->days = 0;
        if($this->hours >= 24)
        {
            $this->days = floor($this->hours / 24);
            $this->hours -= $this->days * 24;
        }
    }

    public function __toString()
    {
        $string = "";
        if($this->hours == 1) $string = $string . $this->hours . " hour, ";
        elseif($this->hours > 1) $string = $string . $this->hours . " hours, ";

        if($this->minutes == 1) $string = $string .  $this->minutes . " minute";
        else $string = $string .  $this->minutes . " minutes";

        return $string;
    }

}