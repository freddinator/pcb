<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['prefix' => 'server'], function() {
    Route::get('{id}/query',        'ServerController@GetStatus');
    Route::get('{id}/data/{interval}',   'ServerController@GetServerStats');
    Route::get('{id}/parse',   function($id) { $stats = new App\Http\Processors\Stats\ServerStats2; $stats->ParseFromBeginning($id); });
    Route::get('{id}/parse_latest',   function($id) { $stats = new App\Http\Processors\Stats\ServerStats2; $stats->ParseLatest($id); });
    Route::get('{id}/data',   function($id) { $stats = new App\Http\Processors\Stats\ServerStats2; return Response::json($stats->Get($id)); });
    Route::get('{id}/economy',   function($id) {
        $stats = new App\Http\Processors\Stats\EconomyStats;
        $stats->PullDifferences();
    });
});

Route::get('/', ['as' => 'home',        function() { return view('master'); }]);

// api routes
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {

    $api->get('news/latest',                'App\Http\Controllers\NewsController@GetLatest');

    $api->group(['prefix' => 'server'], function ($api) {
        $api->get ('{id}/stats',            'App\Http\Controllers\ServerController@GetStats');
        $api->get ('{id}/pcount/{interval}',   'App\Http\Controllers\ServerController@GetPlayerCount');
    });

    $api->group(['prefix' => 'forums'], function ($api) {
        $api->get ('boards',                'App\Http\Controllers\ForumBoardController@GetAll');
        $api->get ('board/{id}',            'App\Http\Controllers\ForumBoardController@Get');
        $api->get ('board/{id}/type',       'App\Http\Controllers\ForumBoardController@GetType');
        $api->post('board/{id}/reorder',    'App\Http\Controllers\ForumBoardController@Reorder');
        $api->get ('category',              'App\Http\Controllers\ForumCategoryController@All');
        $api->post('category/create',       'App\Http\Controllers\ForumCategoryController@Create');
        $api->get ('category/{id}',         'App\Http\Controllers\ForumCategoryController@Get');
        $api->post('category/{id}/save',    'App\Http\Controllers\ForumCategoryController@Save');
        $api->post('category/{id}/reorder', 'App\Http\Controllers\ForumCategoryController@Reorder');
        $api->post('category/{id}/delete',  'App\Http\Controllers\ForumCategoryController@Delete');
        $api->post('thread/create',         'App\Http\Controllers\ForumThreadController@Create');
        $api->get ('thread/latest',         'App\Http\Controllers\ForumThreadController@GetLatest');
        $api->get ('thread/{id}',           'App\Http\Controllers\ForumThreadController@Get');
        $api->post('thread/{id}/lock',      'App\Http\Controllers\ForumThreadController@Lock');
        $api->post('thread/{id}/seal',      'App\Http\Controllers\ForumThreadController@Seal');
        $api->post('thread/{id}/archive',   'App\Http\Controllers\ForumThreadController@Archive');
        $api->post('thread/{id}/sticky',    'App\Http\Controllers\ForumThreadController@Sticky');
        $api->post('thread/{id}/delete',    'App\Http\Controllers\ForumThreadController@Delete');
        $api->post('thread/{id}/unread',    'App\Http\Controllers\ForumThreadController@MarkUnread');
        $api->post('sanitise',              'App\Http\Controllers\ForumThreadController@Sanitise');
        $api->get ('post/latest',           'App\Http\Controllers\ForumPostController@GetLatest');
        $api->post('post/create',           'App\Http\Controllers\ForumPostController@Create');
        $api->post('poll/create',           'App\Http\Controllers\ForumPollController@Create');
        $api->post('poll/{id}/vote',        'App\Http\Controllers\ForumPollController@Vote');
    });

    $api->group(['prefix' => 'ban'], function ($api) {
        $api->get ('list',                  'App\Http\Controllers\BanController@GetList');
    });

    $api->group(['prefix' => 'maintenance'], function ($api) {
        $api->get ('recount/boards',        'App\Http\Controllers\MaintenanceController@RecountBoardStats');
        $api->get ('clear/read',            'App\Http\Controllers\MaintenanceController@ClearRead');
    });

    $api->group(['prefix' => 'auth'], function ($api) {
        $api->post('login',                 'App\Http\Controllers\AccountController@Login');
        $api->get ('exchange',              'App\Http\Controllers\AccountController@TokenToUser');
    });

    $api->group(['prefix' => 'user'], function ($api) {
        $api->get ('roles',                 'App\Http\Controllers\UserController@GetRoles');
        $api->get ('list',                  'App\Http\Controllers\UserController@GetList');
    });

    $api->group(['prefix' => 'servers'], function($api) {
        $api->get ('/',                     'App\Http\Controllers\ServerController@All');
        $api->get ('status',                'App\Http\Controllers\ServerController@GetStatuses');
        $api->post('create',                'App\Http\Controllers\ServerController@Create');
        $api->post('{id}/tag',              'App\Http\Controllers\ServerController@Tag');
        $api->post('{id}/save',             'App\Http\Controllers\ServerController@Save');
        $api->post('{id}/delete',           'App\Http\Controllers\ServerController@Delete');
    });

});


Route::group(['prefix' => 'api'], function() {

    Route::group(['prefix' => 'register'], function() {
        Route::post('check/username',   'RegisterController@CheckUsername');
        Route::post('create',           'RegisterController@CreateAccount');
        Route::post('save',             'RegisterController@SaveDetails');
    });

});