<?php
namespace App\Http\API;

use App\Http\Models\ForumCategory;
use App\Http\Models\ForumBoard;
use App\Http\Models\Setting;
use App\Http\Models\ForumThread;

use DB;
use JWTAuth;
use Input;
use Validator;

use Dingo\Api\Exception\ResourceException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ForumBoardAPI extends BaseAPI {

    /**
     * GET: Returns a list of all categories, their boards and their child boards
     *
     * @return mixed
     */
    public function GetBoardList()
    {
        $categories = ForumCategory::orderBy('order', 'asc')
            ->with([
                'Boards' => function($query) {
                    $query->whereNull('parent_id')
                          ->orderBy('order', 'asc');
                },
                'Boards.ChildBoards' => function($query) {
                    $query->orderBy('order', 'asc');
                }
             ])
            ->with(['Boards.LatestThread', 'Boards.LatestPost'])
            ->get();

        return $categories;
    }

    /**
     * GET: Returns a board's details and it's threads
     *
     * @param $id
     * @return array
     */
    public function GetBoard($id)
    {
        $response = array();

        $board = ForumBoard::where('id', $id)
            ->with([
                'ParentBoard' => function($query) {
                    $query->orderBy('order', 'asc');
                },
                'ChildBoards' => function($query) {
                    $query->orderBy('order', 'asc');
                }
            ])
            ->first();

        if($board == null)
            throw new NotFoundHttpException('Board not found.');


        $user = $this->GetUser();
        $normal_threads = ForumThread::where('board_id', $id)
            ->where('is_stickied', 0)
            ->with(['Root', 'LatestPost']);
        if($user)
        {
            $normal_threads = $normal_threads->with(['Reads' => function ($query) use($user) {
                $query->where('user_id', $user->id);
            }]);
        }
        $normal_threads = $normal_threads->get();

        $stickied_threads = ForumThread::where('board_id', $id)
            ->where('is_stickied', 1)
            ->with(['Root', 'LatestPost']);
        if($user)
        {
            $stickied_threads = $stickied_threads->with(['Reads' => function ($query) use($user) {
                $query->where('user_id', $user->id);
            }]);
        }
        $stickied_threads = $stickied_threads->get();

        // get the setting for: max number of days before a thread is auto displayed as 'read'
        $max_days = Setting::where('key', 'web.read_storage_threshold')->first()->value;

        return [
            'board'     => $board,
            'threads'   => [
                'normal'    => $normal_threads,
                'stickied'  => $stickied_threads
            ],
            'read_storage_threshold' => $max_days
        ];
    }

    public function GetBoardType($id)
    {
        $thread = ForumBoard::find($id);

        if($thread == null)
            throw new NotFoundHttpException('Board not found.');

        return [
            'success'   => true,
            'type'      => $thread->thread_model
        ];
    }

    public function Reorder($id)
    {
        $board = ForumBoard::find($id);
        if($board == null)
            throw new NotFoundHttpException('Board not found.');

        $direction = Input::get('direction');
        $isChild = (bool)Input::get('isChild');
        if($direction == null || $isChild == null)
            throw new ResourceException('Input missing.');

        if($direction !== 1 && $direction !== -1)
            throw new BadRequestHttpException('Invalid Direction input.');

        $new_order = $board->order + $direction;
        $boards_in_category = ForumBoard::where('cat_id', $board->cat_id);
        $boards_in_category = $isChild ? $boards_in_category->whereNull('parent_id') : $boards_in_category->whereNotNull('parent_id');
        $boards_in_category = $boards_in_category->get();

        if($new_order <= 0 || $new_order > count($boards_in_category))
            throw new BadRequestHttpException('Already at the highest position.');

        foreach($boards_in_category as $catBoard)
        {
            if($catBoard->order == $new_order)
            {
                $catBoard->order = $board->order;
                $catBoard->save();
                break;
            }
        }

        $board->order = $new_order;
        $board->save();

        return $this->GetBoardList();
    }
}