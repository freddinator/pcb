<?php
namespace App\Http\API;

use Dingo\Api\Routing\Helpers;

class BaseAPI {
    use Helpers;

    /**
     * Converts authorisation token into user, then refreshes it
     *
     * @return \Illuminate\Auth\GenericUser|\Illuminate\Database\Eloquent\Model
     */
    protected function GetUser()
    {
        $user = $this->auth->user();
        return $user;
    }
}