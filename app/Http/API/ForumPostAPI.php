<?php
namespace App\Http\API;

use App\Http\Models\ForumCategory;
use App\Http\Models\ForumBoard;
use App\Http\Models\ForumPost;
use App\Http\Models\ForumThread;
use App\Http\Models\ForumThreadAction;
use App\Http\Models\ForumAnnouncement;
use App\Http\Models\User;

use Carbon\Carbon;
use DB;
use Dingo\Api\Exception\ResourceException;
use JWTAuth;
use Input;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Validator;
use HTMLPurifier;
use HTMLPurifier_Config;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ForumPostAPI extends BaseAPI {

    public function CreatePost()
    {
        //TODO: do auth check in a serice instead
        $user = JWTAuth::parseToken()->authenticate();

        // TODO: is this even needed? does dingo handle this already?
        if($user == null)
            throw new UnauthorizedHttpException('User token invalid / malformed and therefore not accepted.');

        $thread_id = Input::get('thread_id');
        $parent_id = Input::get('parent_id');
        $parent_type = Input::get('parent_type');
        $message = $this->SanitiseHTML( Input::get('message') );

        // validate input
        $validator = Validator::make([
            'thread_id' => $thread_id,
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'message'   => $message
        ], [
            'thread_id'  => 'required|numeric',
            'parent_id'  => 'required|numeric',
            'parent_type' => 'required',
            'message' => 'required|min:3'
        ]);
        if($validator->fails())
            throw new ResourceException('Invalid form input.', $validator->errors());

        $whitelist = ['post', 'announcement'];
        if(in_array($parent_type, $whitelist, true) === false)
            throw new BadRequestHttpException('Invalid Parent Type.');

        // make sure thread is in a postable state
        $thread = ForumThread::find($thread_id);
        if($thread == null)
            throw new NotFoundHttpException('Thread not found.');

        if($thread->is_locked || $thread->is_sealed || $thread->is_archived)
            throw new AccessDeniedHttpException('You cannot post to a locked, sealed or archived thread.');

        $post = ForumPost::create([
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'message'   => $message,
            'user_id'   => $user->id
        ]);

        if($post == null)
            throw new HttpException('An unexpected error prevented the creation of a new forum posts.');

        $thread->last_post_at = Carbon::now();
        $thread->reply_count += 1;
        $thread->last_post_id = $post->id;
        $thread->save();

        $board = ForumBoard::find($thread->board_id);
        $board->total_posts += 1;
        $board->last_post_id = $post->id;
        $board->last_thread_id = $thread->id;
        $board->save();

        return [
            'success'   => true,
            'id'        => $post->id
        ];
    }

    public function Sanitise()
    {
        $html = Input::get('html');
        return $this->SanitiseHTML($html);
    }

    private function SanitiseHTML($dirty_html)
    {
        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);
        $clean_html = $purifier->purify($dirty_html);

        // remove the 'empty message' boiler tags
        if($clean_html == "<p><br /></p>") return "";

        return $clean_html;
    }

    public function GetLatest()
    {
        $posts = ForumPost::whereNotNull('parent_id')
            ->with(['User', 'Parents'])
            ->orderBy('created_at', 'desc')
            ->take(10)
            ->get();

        // determine the thread by working upwards through the post tree
        $top_posts = array();
        foreach($posts as $post)
        {
            $top_post = null;
            if($post->parents)
            {
                $parent = $post->parents;
                $top_post = ($parent->parent) ? $parent->parent->id : $parent->id;
            }
            else
            {
                $top_post = $post->id;
            }

            if($top_post == null) continue;

            array_push($top_posts, $top_post);
            $post->top_post_id = $top_post;
        }

        // remove duplicates
        $top_posts = array_unique($top_posts);

        // grab all the threads corresponding to the top posts
        $threads = ForumThread::whereIn('root_id', $top_posts)
            ->with('Board')
            ->get();

        foreach($posts as $post)
        {
            foreach($threads as $thread)
            {
                if($thread->root_id == $post->top_post_id)
                {
                    $post->thread = $thread;
                    break;
                }
            }
        }

        return $posts;
    }

}