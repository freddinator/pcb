<?php
namespace App\Http\API;

use App\Http\Models\ForumCategory;
use App\Http\Models\ForumBoard;
use App\Http\Models\ForumPost;
use App\Http\Models\ForumRead;
use App\Http\Models\ForumThread;
use App\Http\Models\ForumThreadAction;
use App\Http\Models\ForumAnnouncement;
use App\Http\Models\User;

use Carbon\Carbon;
use DB;
use Dingo\Api\Exception\ResourceException;
use JWTAuth;
use Input;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Validator;
use HTMLPurifier;
use HTMLPurifier_Config;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ForumThreadAPI extends BaseAPI {

    /**
     * GET: Returns thread data and the posts contained within the thread
     *
     * @param $id
     * @return array
     */
    public function GetThread($id)
    {
        $thread = ForumThread::withTrashed()
            ->with(['Actions', 'Root', 'Poll.Choices.Votes'])
            ->find($id);

        if($thread == null)
            throw new NotFoundHttpException('Thread not found.');

        $thread->view_count += 1;
        $thread->save();

        // get all posts and replies for the root model
        $posts = $thread->root->Posts;

        // get a unique list of user ids
        $user_ids = array();
        foreach($posts as $post)
        {
            array_push($user_ids, $post->user_id);

            foreach($post->replies as $reply)
            {
                array_push($user_ids, $reply->user_id);
            }
        }
        $user_ids = array_unique($user_ids, SORT_NUMERIC);

        // convert user id list into models
        $users = User::whereIn('id', $user_ids)
            ->get();

        // mark thread as 'read' for the user if logged in
        $user = $this->GetUser();
        if($user)
        {
            $read = ForumRead::where('thread_id', $thread->id)
                ->where('user_id', $user->id)
                ->first();

            if($read)
            {
                $read->read_at = Carbon::now();
                $read->save();
            }
            else
            {
                ForumRead::create([
                    'user_id'   => $user->id,
                    'thread_id' => $thread->id,
                    'read_at'   => Carbon::now()
                ]);
            }
        }

        return [
            'thread' => $thread,
            'users' => $users
        ];
    }

    public function GetLatest()
    {
        return ForumThread::with(['Root', 'Board', 'LatestPost'])
            ->orderBy('last_post_at', 'desc')
            ->take(10)
            ->get();
    }

    public function CreateThread()
    {
        //TODO: do auth check in a serice instead
        $user = JWTAuth::parseToken()->authenticate();

        $board = Input::get('board');
        $message = $this->SanitiseHTML( Input::get('message') );
        $title = Input::get('title');
        $type = Input::get('thread_type', 'post');

        $model = null;
        switch($type)
        {
            case 'post':
                //$type = 'App\Http\Models\ForumPost';
                $model = ForumPost::create([
                    'message'   => $message,
                    'user_id'   => $user->id
                ]);
                break;
            case 'announcement':
                //$type = 'App\Http\Models\ForumAnnouncement';
                $model = ForumAnnouncement::create([
                    'message'   => $message,
                    'user_id'   => $user->id
                ]);
                break;
        }

        $thread = ForumThread::create([
            'root_id'           => $model->id,
            'root_type'         => $type,
            'title'             => $title,
            'board_id'          => $board,
            'last_post_at'      => Carbon::now(),
            'last_action_at'    => null
        ]);

        $board = ForumBoard::find($board);
        $board->total_threads += 1;
        $board->last_thread_id = $thread->id;
        $board->last_post_id = null;
        $board->save();

        return [
            'success'   => true,
            'id'        => $thread->id
        ];
    }

    public function Sanitise()
    {
        $html = Input::get('html');
        return $this->SanitiseHTML($html);
    }

    private function SanitiseHTML($dirty_html)
    {
        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);
        $clean_html = $purifier->purify($dirty_html);

        // remove the 'empty message' boiler tags
        if($clean_html == "<p><br /></p>") return "";

        return $clean_html;
    }

    public function SetThreadState($id, $action)
    {
        //TODO: do auth check in a serice instead
        $user = JWTAuth::parseToken()->authenticate();

        $thread = ForumThread::find($id);
        if($thread == null)
            throw new NotFoundHttpException('Thread not found.');

        switch($action)
        {
            case 'locked':
                $new_state = !$thread->is_locked;
                $thread->is_locked = $new_state;
                break;
            case 'sealed':
                $new_state = !$thread->is_sealed;
                $thread->is_sealed = $new_state;
                break;
            case 'archived':
                $new_state = !$thread->is_archived;
                $thread->is_archived = $new_state;
                break;
            case 'stickied':
                $new_state = !$thread->is_stickied;
                $thread->is_stickied = $new_state;
                break;
            default:
                return ['success' => false];
        }
        $thread->last_action_at = Carbon::now();
        $thread->save();

        $action = ForumThreadAction::create([
            'thread_id'     => $id,
            'user_id'       => $user->id,
            'action'        => $action,
            'is_active'     => $new_state,
            'created_at'    => Carbon::now()
        ]);

        return [
            'success' => true
        ];
    }

    public function MarkUnread($id)
    {
        //TODO: do auth check in a serice instead
        $user = JWTAuth::parseToken()->authenticate();

        $thread = ForumThread::find($id);
        if($thread == null)
            throw new NotFoundHttpException('Thread not found.');

        ForumRead::where('thread_id', $thread->id)
            ->where('user_id', $user->id)
            ->delete();

        return [
            'success' => true
        ];
    }

    /**
     * Soft-deletes or restores a thread
     *
     * @param $id
     * @return array
     */
    public function DeleteThread($id)
    {
        //TODO: merge with SetThreadState()
        //TODO: do auth check in a serice instead
        $user = JWTAuth::parseToken()->authenticate();

        $delete = Input::get('delete');

        $thread = ForumThread::withTrashed()->find($id);

        if($thread == null)
            throw new NotFoundHttpException('Thread not found.');

        $thread->last_action_at = Carbon::now();
        $thread->save();

        if($delete)
            $thread->delete();
        else
            $thread->restore();

        $action = ForumThreadAction::create([
            'thread_id'     => $id,
            'user_id'       => $user->id,
            'action'        => 'deleted',
            'is_active'     => $delete,
            'created_at'    => Carbon::now()
        ]);

        return [
            'success' => true
        ];
    }

}