<?php
namespace App\Http\API;

use App\Http\Models\ForumCategory;
use App\Http\Models\ForumBoard;
use App\Http\Models\ForumPost;
use App\Http\Models\ForumThread;
use App\Http\Models\ForumThreadAction;
use App\Http\Models\ForumAnnouncement;
use App\Http\Models\User;

use Carbon\Carbon;
use DB;
use Dingo\Api\Exception\ResourceException;
use JWTAuth;
use Input;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Validator;
use HTMLPurifier;
use HTMLPurifier_Config;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class UserAPI extends BaseAPI {

    public function GetRoles()
    {
        $user = User::with('Roles.Permissions')
            ->find(2);

        $permissions = array();
        foreach($user->Roles as $role)
        {
            foreach($role->Permissions as $permission)
            {
                array_push($permissions, $permission->permission);
            }
        }

        $user->permissions = array_unique($permissions);

        $hidden = array_merge($user->getHidden(), ['roles.permissions']);
        $user->setHidden($hidden);

        return $user;
    }

    public function GetList()
    {
        $users = User::with('Roles')
            ->get();

        return $users;
    }

}