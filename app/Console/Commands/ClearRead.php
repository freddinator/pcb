<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Cache;
use Route;

/**
 * Clears 'topic read' entries older than X days
 *
 * Class ClearRead
 * @package App\Console\Commands
 */
class ClearRead extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ClearRead';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears "topic read" entries older than X days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $request = Request::create('http://localhost/PCB_update/public/api/forums/board/1', 'GET');
        echo $request;
        $response = Route::dispatch($request);
        echo $response;
    }
}
