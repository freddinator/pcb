<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Models\Server;
use App\Http\Models\ServerStatus;
use Cache;

class GrabLive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GrabLive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab live data (during development) from the live web server database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = 1;

        $live_server = new ServerStatus;
        $live_server->setConnection('live_server');

        $statuses = $live_server->where('server_id', $id)->get();

        $local_server = new ServerStatus;
        $local_server->setConnection('mysql');
        $local_server->where('server_id', $id)->delete();

        $data = array();
        foreach($statuses as $status)
        {
            array_push($data, [
                'server_id'         => $status->server_id,
                'is_online'         => $status->is_online,
                'current_players'   => $status->current_players,
                'max_players'       => $status->max_players,
                'players'           => $status->players,
                'date'              => $status->date
            ]);
        }
        $local_server->insert($data);


        // flush cache
        $server = Server::find($id);
        $row = $server->LatestStatus;
        $key = 'server_status_' . $id;
        Cache::forget($key);
        Cache::forever($key, $row);
    }
}
