/**
 * User authorisation handling service. Handles:
 *      - local token storage and retrieval
 *      - authorisation header setting
 *      - token and user retrieval via web api
 */
export default {

    storage_key: 'id_token',

    /**
     * POST: attempts to login with the given credentials via the web api
     */
    retrieveToken: function(context, credentials, onSuccess, onInvalid) {
        var self = this;
        context.$http.post('auth/login', credentials).then(function(response) {
            var data = response.data;
            if(data.success) {
                self.setStorage(data.token);
                onSuccess(data.user);
            } else {
                onInvalid();
            }
        }, function(response) {
            console.log(response);
        });
    },

    /**
     * GET: exchanges token in header for user details
     */
    retrieveUser: function(context, onSuccess, onError) {
        if(!this.hasStorage()) {
            return;
        }

        context.$http.get('auth/exchange').then(function(response) {
            if(response.data)
            {
                onSuccess(response.data);
            }
        }, function(response) {
            onError(response);
        });
    },

    /**
     * Checks for a token in storage, and puts it into the header.
     * This should be done everytime at boot!
     */
    pushHeader: function(context) {
        var self = this;
        context.http.interceptors.push({
            request: function (request) {
                var token = self.getStorage();
                if(token) {
                    request.headers['Authorization'] = 'Bearer ' + token;
                }
                return request;
            }
        });
    },

    /**
     * Returns whether a token is in storage
     */
    hasStorage: function() {
        if(this.getStorage())
            return true;

        return false;
    },

    /**
     * Stores a token locally (as a cookie)
     */
    setStorage: function(token) {
        localStorage.setItem(this.storage_key, token);
    },

    getStorage: function() {
        return localStorage.getItem(this.storage_key);
    },

    clearStorage: function() {
        localStorage.removeItem(this.storage_key);
    }

}