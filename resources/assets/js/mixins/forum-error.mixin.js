export default {
    methods: {
        // bubble upwards an AJAX response error
        broadcastError: function(response) {
            this.$dispatch('forum-error', response);
        }
    }
}