<?php

use Illuminate\Database\Seeder;

class ServerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $server1 = DB::table('servers')->insert([
            'name'  => 'Survival / Creative',
            'ip'    => '198.144.156.53',
            'ip_alias' => 'pcbmc.co',
            'port'  => '25565',
            'type'  => 'minecraft'
        ]);

        $server2 = DB::table('servers')->insert([
            'name'  => 'Teamspeak',
            'ip'    => '199.241.30.5',
            'port'  => '9987',
            'type'  => 'teamspeak'
        ]);

        $server3 = DB::table('servers')->insert([
            'name'  => 'Feed The Beast',
            'ip'    => '63.141.245.210',
            'port'  => '25565',
            'type'  => 'minecraft',
            'show_port' => true,
        ]);

        $server4 = DB::table('servers')->insert([
            'name'  => 'Pixelmon',
            'ip'    => '63.141.245.212',
            'port'  => '25566',
            'type'  => 'minecraft',
            'show_port' => true,
        ]);

    }
}
