<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEconomyTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('economy_transactions', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('shop_user_id')->unsigned()->nullable();
            $table->string('shop_uuid');
            $table->integer('customer_user_id')->unsigned()->nullable();
            $table->string('customer_uuid');
            $table->string('item');
            $table->string('item_sub_id')->nullable();
            $table->enum('transaction_type', ['bought', 'sold']);
            $table->integer('amount')->unsigned();
            $table->integer('quantity')->unsigned();
            $table->timestamp('time');

            $table->foreign('shop_user_id')->references('id')->on('users');
            $table->foreign('customer_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('economy_transactions');
    }
}
