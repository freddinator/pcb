<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_stats', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('server_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('username');
            $table->integer('mins_online');
            $table->timestamp('last_online');

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('player_stats');
    }
}
