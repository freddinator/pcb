<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanlist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banlist', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('server_id')->unsigned();
            $table->boolean('is_active');
            $table->boolean('is_global_ban');
            $table->integer('staff_id')->unsigned()->nullable();
            $table->string('staff_name');
            $table->string('staff_uuid');
            $table->string('player_name');
            $table->string('player_uuid');
            $table->string('ban_reason');
            $table->timestamp('ban_date');
            $table->timestamp('unban_date');

            $table->foreign('server_id')->references('id')->on('servers');
            $table->foreign('staff_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banlist');
    }
}
