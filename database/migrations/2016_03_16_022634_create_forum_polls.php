<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumPolls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_polls', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('thread_id')->unsigned();
            $table->string('question');
            $table->timestamp('locks_at')->nullable();
            $table->integer('max_choices')->default(1);
            $table->boolean('vote_requires_post');
            $table->boolean('anonymous_votes');
            $table->boolean('votes_visible');
            $table->timestamps();

            $table->foreign('thread_id')->references('id')->on('forum_threads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forum_polls');
    }
}
