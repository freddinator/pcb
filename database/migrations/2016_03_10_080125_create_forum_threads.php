<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumThreads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_threads', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('board_id')->unsigned();
            $table->morphs('root');
            $table->string('title')->nullable();
            $table->integer('view_count')->unsigned();
            $table->integer('reply_count')->unsigned();
            $table->boolean('is_locked');
            $table->boolean('is_sealed');
            $table->boolean('is_stickied');
            $table->boolean('is_archived');
            $table->timestamp('last_post_at');
            $table->timestamp('last_action_at')->nullable();
            $table->softDeletes();

            $table->foreign('board_id')->references('id')->on('forum_boards');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forum_threads');
    }
}
