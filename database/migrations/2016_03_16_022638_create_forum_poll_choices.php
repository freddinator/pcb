<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumPollChoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_poll_choices', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('poll_id')->unsigned();
            $table->string('choice');

            $table->foreign('poll_id')->references('id')->on('forum_polls');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forum_poll_choices');
    }
}
