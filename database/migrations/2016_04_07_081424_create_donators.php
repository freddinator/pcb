<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('ranked_user_id')->unsigned();
            $table->decimal('amount')->unsigned();
            $table->boolean('is_anonymous');
            $table->timestamp('donated_at');
            $table->timestamp('expires_at')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('ranked_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('donations');
    }
}
