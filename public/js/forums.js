
loadjs.d("./resources/assets/js/routes/forums.board.route.vue",function(require,module,exports){
var __vueify_style__ = require("vueify-insert-css").insert("\n.forum-threads > tbody > tr > td {\n    line-height: 25px;\n    min-height: 35px;\n    height: 35px;\n    vertical-align: middle;\n    padding-top: 10px;\n    padding-bottom: 10px;\n}\n\n.forum-threads .glyphicon {\n    font-size: 1.5em;\n}\n\n.large-text {\n    font-size: 1.2em;\n}\n")
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _forumCategory = require('../components/forums/forum-category.vue');

var _forumCategory2 = _interopRequireDefault(_forumCategory);

var _forumErrorMixin = require('../mixins/forum-error.mixin.js');

var _forumErrorMixin2 = _interopRequireDefault(_forumErrorMixin);

var _user = require('../stores/user.js');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var moment = require('moment');

exports.default = {
    data: function data() {
        return {
            board: [],
            normalThreads: [],
            stickiedThreads: [],
            childBoards: [],
            readThreshold: 0,

            orderDirection: -1,
            orderField: 'last_post_at',

            userState: _user2.default.state
        };
    },


    mixins: [_forumErrorMixin2.default],

    computed: {
        hasChildBoards: function hasChildBoards() {
            return this.childBoards.length > 0;
        }
    },

    components: {
        'forum-category': _forumCategory2.default
    },

    methods: {
        getData: function getData(callback) {
            var self = this;
            self.$http.get('forums/board/' + this.$route.params.board).then(function (response) {
                this.board = response.data.board;
                this.normalThreads = response.data.threads.normal;
                this.stickiedThreads = response.data.threads.stickied;
                this.childBoards = response.data.board.child_boards;
                this.readThreshold = response.data.read_storage_threshold;

                this.checkIfNew(self.normalThreads);
                this.checkIfNew(self.stickiedThreads);

                if (callback != null) {
                    callback();
                }
            }, function (response) {
                this.broadcastError(response);
            });
        },

        sortBy: function sortBy(field) {
            if (this.orderField == field) {
                this.orderDirection *= -1;
            } else {
                this.orderDirection = -1;
                this.orderField = field;
            }
        },

        checkIfNew: function checkIfNew(arr) {
            var maxDate = moment().subtract(this.readThreshold, 'days');
            for (var i = 0; i < arr.length; i++) {
                // threads with post activity older than the threshold is auto considered 'read'
                if (!this.userState.authorised || moment(arr[i].last_post_at).isSameOrBefore(maxDate)) {
                    arr[i].isNew = false;
                    continue;
                }

                // otherwise check if the read date was before the latest post
                if (!jQuery.isEmptyObject(arr[i].reads)) {
                    arr[i].isNew = moment(arr[i].reads[0].read_at).isBefore(arr[i].last_post_at);
                } else {
                    arr[i].isNew = true;
                }
            }
        },

        getTimeAgo: function getTimeAgo(time) {
            return moment(time).fromNow();
        }
    },

    ready: function ready() {
        this.$dispatch('onTabSelected', 'forums');
    },

    route: {
        activate: function activate(_ref) {
            var next = _ref.next;

            this.getData(next);
        },

        canReuse: function canReuse(transition) {
            if (transition.to.path != transition.from.path) {
                if (transition.to.params.board != transition.from.params.board) {
                    return false;
                }
            }

            return true;
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div>\n    <div class=\"col-sm-12\">\n        <div class=\"panel panel-default top-bar\">\n            <div class=\"panel-body\">\n                <div class=\"pull-right\">\n                    <a class=\"btn btn-primary btn-lg\" v-link=\"{path: '/forums/board/' + board.id + '/topic'}\">\n                        <span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> New Topic\n                    </a>\n                </div>\n\n                <h4>{{board.name}}</h4>\n\n                <forum-category v-if=\"hasChildBoards\" :name=\"'Child Boards'\" :boards=\"childBoards\">\n                </forum-category>\n\n                <hr>\n\n                <div class=\"col-sm-6\">\n                    <strong>Topics</strong>\n                </div>\n                <div class=\"col-sm-6\">\n                    <ul class=\"nav nav-tabs nav-justified\">\n                        <li role=\"presentation\" v-bind:class=\"{'active': orderField == 'last_post_at'}\"><a href=\"\" v-on:click.prevent=\"sortBy('last_post_at')\">Most Recent</a></li>\n                        <li role=\"presentation\" v-bind:class=\"{'active': orderField == 'reply_count'}\"><a href=\"\" v-on:click.prevent=\"sortBy('reply_count')\">Most Replied</a></li>\n                        <li role=\"presentation\" v-bind:class=\"{'active': orderField == 'view_count'}\"><a href=\"\" v-on:click.prevent=\"sortBy('view_count')\">Most Viewed</a></li>\n                    </ul>\n                </div>\n\n                <table class=\"table table-responsive forum-threads\">\n                    <tbody>\n                        <tr v-for=\"thread in stickiedThreads | orderBy orderField orderDirection\" class=\"info\">\n                            <td class=\"text-center\">\n                                <span class=\"glyphicon glyphicon-pushpin\" aria-hidden=\"true\"></span>\n                            </td>\n                            <td>\n                                <a href=\"\" v-link=\"{path: '/forums/topic/' + thread.id}\" class=\"large-text\">\n                                    {{thread.title}}\n                                    <span class=\"label label-warning\" v-if=\"thread.isNew\">New</span>\n                                </a><br>\n                                42 minutes ago by Andy\n                            </td>\n                            <td class=\"text-center\">\n                                <span class=\"large-text\"><strong>{{thread.reply_count}}</strong></span><br>\n                                Replies\n                                </td>\n                            <td class=\"text-center\">\n                                <span class=\"large-text\"><strong>{{thread.view_count}}</strong></span><br>\n                                Views</td>\n                            <td>\n                                <template v-if=\"thread.latest_post\">\n                                    <img class=\"media-object\" src=\"https://minotar.net/avatar/_andy/32.png\">\n                                    Last post by <a href=\"#\" v-link=\"{path:'/user/' + thread.latest_post.user_id}\">someone</a><br>\n                                    {{getTimeAgo(thread.last_post_at)}}\n                                </template>\n                                <template v-else=\"\">\n                                    No replies.\n                                </template>\n                            </td>\n                        </tr>\n                        <tr v-for=\"thread in normalThreads | orderBy orderField orderDirection\" v-bind:class=\"{'active': thread.is_locked}\">\n                            <td class=\"text-center\">\n                                <template v-if=\"thread.is_locked\">\n                                    <span class=\"glyphicon glyphicon-lock\" aria-hidden=\"true\"></span>\n                                </template>\n                                <template v-else=\"\">\n                                    <span class=\"glyphicon glyphicon-unchecked\" aria-hidden=\"true\"></span>\n                                </template>\n                            </td>\n                            <td>\n                                <a href=\"\" v-link=\"{path: '/forums/topic/' + thread.id}\" class=\"large-text\">\n                                    {{thread.title}}\n                                    <span class=\"label label-warning\" v-if=\"thread.isNew\">New</span>\n                                </a><br>\n                                {{getTimeAgo(thread.root.created_at)}} by Andy\n                            </td>\n                            <td class=\"text-center\">\n                                <span class=\"large-text\"><strong>{{thread.reply_count}}</strong></span><br>\n                                Replies\n                                </td>\n                            <td class=\"text-center\">\n                                <span class=\"large-text\"><strong>{{thread.view_count}}</strong></span><br>\n                                Views</td>\n                            <td>\n                                <template v-if=\"thread.latest_post\">\n                                    <img class=\"media-object\" src=\"https://minotar.net/avatar/_andy/32.png\">\n                                    Last post by <a href=\"#\" v-link=\"{path:'/user/' + thread.latest_post.user_id}\">someone</a><br>\n                                    {{getTimeAgo(thread.last_post_at)}}\n                                </template>\n                                <template v-else=\"\">\n                                    No replies.\n                                </template>\n                            </td>\n                        </tr>\n\n                        <tr>\n                            <td colspan=\"5\">Nothing here. You could be the first to <a href=\"\" v-link=\"{path: '/forums/board/' + board.id + '/topic'}\">start a thread</a>!</td>\n                        </tr>\n\n                    </tbody>\n                </table>\n\n            </div>\n        </div>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\forums.board.route.vue"
  module.hot.dispose(function () {
    require("vueify-insert-css").cache["\n.forum-threads > tbody > tr > td {\n    line-height: 25px;\n    min-height: 35px;\n    height: 35px;\n    vertical-align: middle;\n    padding-top: 10px;\n    padding-bottom: 10px;\n}\n\n.forum-threads .glyphicon {\n    font-size: 1.5em;\n}\n\n.large-text {\n    font-size: 1.2em;\n}\n"] = false
    document.head.removeChild(__vueify_style__)
  })
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"../components/forums/forum-category.vue":36,"../mixins/forum-error.mixin.js":47,"../stores/user.js":67,"moment":4,"vue":33,"vue-hot-reload-api":6,"vueify-insert-css":34});

loadjs.d("./resources/assets/js/routes/forums.index.route.vue",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _forumCategory = require('../components/forums/forum-category.vue');

var _forumCategory2 = _interopRequireDefault(_forumCategory);

var _forumErrorMixin = require('../mixins/forum-error.mixin.js');

var _forumErrorMixin2 = _interopRequireDefault(_forumErrorMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            categories: []
        };
    },


    mixins: [_forumErrorMixin2.default],

    components: {
        'forum-category': _forumCategory2.default
    },

    methods: {
        getBoards: function getBoards() {
            this.$http.get('forums/boards').then(function (response) {
                this.categories = response.data.forum_categories;
            }, function (response) {
                this.broadcastError(response);
            });
        }
    },

    ready: function ready() {
        this.$dispatch('onTabSelected', 'forums');
    },

    route: {
        activate: function activate() {
            this.getBoards();
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div>\n    <div class=\"col-sm-12\">\n        <div class=\"panel panel-default top-bar\">\n            <div class=\"panel-body\">\n\n                <forum-category v-for=\"category in categories\" :name=\"category.name\" :boards=\"category.boards\">\n                </forum-category>\n\n            </div>\n        </div>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\forums.index.route.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"../components/forums/forum-category.vue":36,"../mixins/forum-error.mixin.js":47,"vue":33,"vue-hot-reload-api":6});

loadjs.d("./resources/assets/js/routes/forums.memberlist.route.vue",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _forumErrorMixin = require('../mixins/forum-error.mixin.js');

var _forumErrorMixin2 = _interopRequireDefault(_forumErrorMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            users: []
        };
    },


    mixins: [_forumErrorMixin2.default],

    methods: {
        getUsers: function getUsers() {
            this.$http.get('user/list').then(function (response) {
                this.users = response.data.users;
            }, function (response) {
                this.broadcastError(response);
            });
        },

        getDisplayName: function getDisplayName(user) {
            return user.alias == null ? user.username : user.alias;
        },

        getRoles: function getRoles(user) {
            var roles = [];
            for (var i = 0; i < user.roles.length; i++) {
                roles.push(user.roles[i].name);
            }
            return roles.join(", ");
        },

        getAvatar: function getAvatar(user) {
            return 'https://minotar.net/avatar/' + this.getDisplayName(user) + '/16.png';
        }
    },

    route: {
        activate: function activate() {
            this.getUsers();
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div>\n    <div class=\"col-sm-12\">\n        <div class=\"panel panel-default top-bar\">\n            <div class=\"panel-heading\">\n                <nav>\n                    <ul class=\"pagination\">\n                        <li>\n                            <a href=\"#\" aria-label=\"Previous\">\n                                <span aria-hidden=\"true\">«</span>\n                            </a>\n                        </li>\n                        <li><a href=\"#\">1</a></li>\n                        <li><a href=\"#\">2</a></li>\n                        <li><a href=\"#\">3</a></li>\n                        <li><a href=\"#\">4</a></li>\n                        <li><a href=\"#\">5</a></li>\n                        <li>\n                            <a href=\"#\" aria-label=\"Next\">\n                                <span aria-hidden=\"true\">»</span>\n                            </a>\n                        </li>\n                    </ul>\n                </nav>\n            </div>\n            <div class=\"panel-body\">\n                <table class=\"table table-striped\">\n                    <thead>\n                        <tr>\n                            <td></td>\n                            <td>Display Name</td>\n                            <td>Role(s)</td>\n                            <td>Member Since</td>\n                            <td>Threads</td>\n                            <td>Posts</td>\n                        </tr>\n                    </thead>\n                    <tbody>\n                    <tr v-for=\"user in users\">\n                        <td><img v-bind:src=\"getAvatar(user)\"></td>\n                        <td><a href=\"\" v-link=\"{path: '/user/' + user.id}\">{{getDisplayName(user)}}</a></td>\n                        <td>{{getRoles(user)}}</td>\n                        <td>{{user.created_at}}</td>\n                        <td></td>\n                        <td></td>\n                    </tr>\n                    </tbody>\n                </table>\n            </div>\n            <div class=\"panel-footer\">\n                test\n            </div>\n        </div>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\forums.memberlist.route.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"../mixins/forum-error.mixin.js":47,"vue":33,"vue-hot-reload-api":6});

loadjs.d("./resources/assets/js/routes/forums.new.thread.route.vue",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    data: function data() {
        return {
            title: '',
            preview: '',
            thread_model: 'post'
        };
    },


    ready: function ready() {
        this.$dispatch('onTabSelected', 'forums');
        var self = this;

        $('#summernote').summernote({
            height: 250,
            minHeight: 150,
            focus: true,
            placeholder: 'Write something here...'
        });
    },

    methods: {
        getModelType: function getModelType() {
            var self = this;
            this.$http.get('forums/board/' + this.$route.params.board + '/type').then(function (response) {
                console.log(response);
                var data = response.data;
                if (data.success) {
                    self.thread_model = data.type;
                }
            }, function (response) {
                console.log(response);
            });
        },

        createTopic: function createTopic() {
            var data = {
                board: this.$route.params.board,
                message: $('#summernote').summernote('code'),
                title: this.title,
                thread_type: this.thread_model
            };

            var $btn = $('#btnCreate').button('loading');

            var self = this;
            this.$http.post('forums/thread/create', data).then(function (response) {
                var data = response.data;
                if (data.success) {
                    self.$router.go('/forums/topic/' + data.id);
                }
                $btn.button('reset');
            }, function (response) {
                console.log(response);
                $btn.button('reset');
            });
        },

        getPreview: function getPreview() {
            var data = {
                html: $('#summernote').summernote('code')
            };

            var self = this;
            this.$http.post('forums/sanitise', data).then(function (response) {
                self.preview = response.data;
            }, function (response) {
                console.log(response);
            });
        }
    },

    route: {
        activate: function activate() {
            this.getModelType();
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div>\n    <div class=\"col-sm-12\">\n\n        <form action=\"post\">\n        <div class=\"panel panel-primary\">\n            <div class=\"panel-heading\">\n                New Topic\n            </div>\n\n            <div class=\"panel-body\">\n                <div class=\"form-group\">\n                  <label for=\"exampleInputEmail1\">Title</label>\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Thread Title\" v-model=\"title\">\n                </div>\n\n                <div id=\"summernote\"></div>\n            </div>\n\n            <div class=\"panel-footer text-right\">\n                <button type=\"button\" class=\"btn btn-info\" v-on:click=\"getPreview\">\n                    <span class=\"glyphicon glyphicon-chevron-down\" aria-hidden=\"true\"></span> Preview\n                </button>\n                <div class=\"btn-group\">\n                    <button type=\"button\" class=\"btn btn-primary\" v-on:click=\"createTopic\" id=\"btnCreate\" data-loading-text=\"Posting...\">\n                        <span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> Create Topic\n                    </button>\n                    <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                        <span class=\"caret\"></span>\n                        <span class=\"sr-only\">Toggle Dropdown</span>\n                    </button>\n                    <ul class=\"dropdown-menu\">\n                        <li><a href=\"#\">Save to Drafts</a></li>\n                    </ul>\n                </div>\n            </div>\n        </div>\n        </form>\n\n\n        <div class=\"panel panel-info\" v-show=\"preview\">\n            <div class=\"panel-heading\">\n                <div class=\"row\">\n                    <div class=\"col-sm-8\">\n                        <h3>{{title}}</h3>\n                    </div>\n                    <div class=\"col-sm-4 text-right\">\n                        <ul class=\"list-inline text-center\">\n                            <li>\n                                <dd>Replies\n                                </dd><dt>42</dt>\n                            </li>\n                            <li>\n                                <dd>Posters\n                                </dd><dt>17</dt>\n                            </li>\n                            <li>\n                                <dd>Views\n                                </dd><dt>10,322</dt>\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"panel-body forum-post\">\n                <div class=\"col-sm-2 text-center\">\n                    <span class=\"label label-success\">Online</span>\n                    <p>\n                    <img src=\"https://minotar.net/avatar/_andy/64.png\" alt=\"...\" class=\"img-rounded\">\n                    </p><p>\n                    <span class=\"label label-default\">Admin</span>\n                    <span class=\"label label-default\">Donator</span>\n\n                    </p><hr>\n\n                    <div class=\"user-stats\">\n                        <span class=\"pull-right\">1,724</span>\n                        <div class=\"text-left\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> Posts</div>\n\n                        <span class=\"pull-right\">438</span>\n                        <div class=\"text-left\"><span class=\"glyphicon glyphicon-file\" aria-hidden=\"true\"></span> Topics</div>\n\n                        <span class=\"pull-right\">192</span>\n                        <div class=\"text-left\"><span class=\"glyphicon glyphicon-thumbs-up\" aria-hidden=\"true\"></span> Thumbs</div>\n                    </div>\n\n                    \n                    <p>\n                    <span class=\"glyphicon glyphicon-text-background\" aria-hidden=\"true\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Former Names:\"></span>\n                    <span class=\" glyphicon glyphicon-king\" aria-hidden=\"true\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Events Won:\"></span>\n                </p></div>\n                <div class=\"col-sm-10\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-8\">\n                            <p class=\"username\"><a href=\"\">Andy</a></p>\n                        </div>\n                        <div class=\"col-sm-4 text-right\">\n                            <span class=\"date\">17 days ago</span>\n                        </div>\n                    </div>\n\n                    {{{preview}}}\n\n                    <hr>\n\n                    Signature\n                </div>\n            </div>\n            <div class=\"panel-footer\">\n                <div class=\"row\">\n                    <div class=\"col-sm-8\">\n                        <ul class=\"list-inline liked-users\">\n                            <li>Liked by 17 users</li>\n                            <li><img src=\"https://minotar.net/avatar/_andy/16.png\"></li>\n                            <li><img src=\"https://minotar.net/avatar/_andy/16.png\"></li>\n                            <li><img src=\"https://minotar.net/avatar/_andy/16.png\"></li>\n                            <li><img src=\"https://minotar.net/avatar/_andy/16.png\"></li>\n                            <li><img src=\"https://minotar.net/avatar/_andy/16.png\"></li>\n                        </ul>\n                    </div>\n                    <div class=\"col-sm-4 text-right\">\n                        <div class=\"btn-group\" role=\"group\" aria-label=\"...\">\n                            <button type=\"button\" class=\"btn btn-default\">\n                                <span class=\"glyphicon glyphicon-thumbs-up\" aria-hidden=\"true\"></span> like\n                            </button>\n                            <button type=\"button\" class=\"btn btn-default\">\n                                <span class=\"glyphicon glyphicon-arrow-down\" aria-hidden=\"true\"></span> new post\n                            </button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\forums.new.thread.route.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"vue":33,"vue-hot-reload-api":6});

loadjs.d("./resources/assets/js/routes/forums.route.vue",function(require,module,exports){
var __vueify_style__ = require("vueify-insert-css").insert("\n.fade-transition[_v-753d944f]\n{\n  -webkit-transition: opacity .1s ease;\n  transition: opacity .1s ease;\n}\n.fade-enter[_v-753d944f], .fade-leave[_v-753d944f]\n{\n  opacity: 0;\n}\n\n.breadcrumb[_v-753d944f]\n{\n    margin: 0;\n    padding: 0;\n    border: 0;\n    background: none;\n}\n\n.no-padding[_v-753d944f]\n{\n    margin: 0;\n    padding: 0;\n}\n")
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _forumAlerts = require('../components/forums/forum-alerts.vue');

var _forumAlerts2 = _interopRequireDefault(_forumAlerts);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            errors: []
        };
    },


    components: {
        'forum-alerts': _forumAlerts2.default
    },

    events: {
        // catch any remaining unhandled AJAX errors for the forums
        'forum-error': function forumError(response) {
            this.errors = [{
                type: 'error',
                message: response.data.message,
                code: response.status
            }];
        }
    }

};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div _v-753d944f=\"\">\n    <div class=\"row\" _v-753d944f=\"\">\n        <div class=\"col-sm-12\" _v-753d944f=\"\">\n            <div class=\"panel panel-default top-bar\" _v-753d944f=\"\">\n                <div class=\"panel-body\" _v-753d944f=\"\">\n\n                    <div class=\"col-sm-8 no-padding\" _v-753d944f=\"\">\n                        <ul class=\"nav nav-pills\" _v-753d944f=\"\">\n                            <li role=\"presentation\" class=\"active\" _v-753d944f=\"\"><a href=\"\" v-link=\"{path: '/forums'}\" _v-753d944f=\"\">Forums</a></li>\n                            <li role=\"presentation\" _v-753d944f=\"\"><a href=\"#\" _v-753d944f=\"\">Activity Feed</a></li>\n                            <li role=\"presentation\" _v-753d944f=\"\"><a href=\"#\" _v-753d944f=\"\">Stats</a></li>\n                            <li role=\"presentation\" class=\"dropdown\" _v-753d944f=\"\">\n                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" _v-753d944f=\"\">\n                                    More <span class=\"caret\" _v-753d944f=\"\"></span>\n                                </a>\n                                <ul class=\"dropdown-menu\" _v-753d944f=\"\">\n                                    <li role=\"presentation\" _v-753d944f=\"\"><a href=\"#\" v-link=\"{path: '/forums/memberlist'}\" _v-753d944f=\"\">Member List</a></li>\n                                    <li role=\"presentation\" _v-753d944f=\"\"><a href=\"#\" _v-753d944f=\"\">Search</a></li>\n                                </ul>\n                            </li>\n                        </ul>\n                    </div>\n\n                    <div class=\"col-sm-4 no-padding\" _v-753d944f=\"\">\n                        <div class=\"progress\" _v-753d944f=\"\">\n                            <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%;\" _v-753d944f=\"\">\n                                <span class=\"sr-only\" _v-753d944f=\"\">60% Complete</span>\n                            </div>\n                        </div>\n                    </div>\n\n                </div>\n                <div class=\"panel-footer\" _v-753d944f=\"\">\n                    <div class=\"pull-right\" _v-753d944f=\"\">\n                        search\n                    </div>\n\n                    <ol class=\"breadcrumb\" _v-753d944f=\"\">\n                        <li _v-753d944f=\"\"><a href=\"#\" v-link=\"{path: '/forums/'}\" _v-753d944f=\"\">Board Categories</a></li>\n                        <li _v-753d944f=\"\"><a href=\"#\" _v-753d944f=\"\">{{boardName}}</a></li>\n                    </ol>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"row\" _v-753d944f=\"\">\n        <div class=\"col-sm-12\" _v-753d944f=\"\">\n            <forum-alerts :queue=\"errors\" _v-753d944f=\"\"></forum-alerts>\n        </div>\n    </div>\n\n    <div class=\"row\" _v-753d944f=\"\">\n        <router-view transition=\"fade\" transition-mode=\"out-in\" _v-753d944f=\"\"></router-view>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\forums.route.vue"
  module.hot.dispose(function () {
    require("vueify-insert-css").cache["\n.fade-transition[_v-753d944f]\n{\n  -webkit-transition: opacity .1s ease;\n  transition: opacity .1s ease;\n}\n.fade-enter[_v-753d944f], .fade-leave[_v-753d944f]\n{\n  opacity: 0;\n}\n\n.breadcrumb[_v-753d944f]\n{\n    margin: 0;\n    padding: 0;\n    border: 0;\n    background: none;\n}\n\n.no-padding[_v-753d944f]\n{\n    margin: 0;\n    padding: 0;\n}\n"] = false
    document.head.removeChild(__vueify_style__)
  })
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"../components/forums/forum-alerts.vue":35,"vue":33,"vue-hot-reload-api":6,"vueify-insert-css":34});

loadjs.d("41",function(require,module,exports){
var __vueify_style__ = require("vueify-insert-css").insert("\n.cover {\n    background-size: cover;\n    background-repeat: no-repeat;\n    background-image: url('http://localhost/PCB_update/public/img/news_img.jpg');\n    height: 100px;\n}\n\n")
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    props: ['thread', 'root']
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div class=\"panel panel-info\">\n    <div class=\"panel-heading\">\n        <div class=\"row\">\n            <div class=\"col-sm-8\">\n                <h3><span class=\"glyphicon glyphicon-pushpin\" v-if=\"thread.is_stickied\" aria-hidden=\"true\"></span></h3>\n            </div>\n            <div class=\"col-sm-4 text-right\">\n                <ul class=\"list-inline text-center\">\n                    <li>\n                        <dt>{{thread.reply_count}}</dt>\n                        <dd>Replies\n                    </dd></li>\n                    <li>\n                        <dt>17</dt>\n                        <dd>Posters\n                    </dd></li>\n                    <li>\n                        <dt>{{thread.view_count}}</dt>\n                        <dd>Views\n                    </dd></li>\n                </ul>\n            </div>\n        </div>\n    </div>\n\n    <ul class=\"list-group\">\n        <li class=\"list-group-item cover\">\n        </li>\n    </ul>\n\n    <div class=\"panel-body forum-post\">\n        <h2>{{thread.title}}</h2>\n        <p>Posted by Andy (5 days ago)</p>\n\n        {{{root.message}}}\n\n    </div>\n    <div class=\"panel-footer\">\n        <div class=\"row\">\n            <div class=\"col-sm-8\">\n                <ul class=\"list-inline liked-users\">\n                    <li>Liked by 17 users</li>\n                    <li><img src=\"https://minotar.net/avatar/_andy/16.png\"></li>\n                    <li><img src=\"https://minotar.net/avatar/_andy/16.png\"></li>\n                    <li><img src=\"https://minotar.net/avatar/_andy/16.png\"></li>\n                    <li><img src=\"https://minotar.net/avatar/_andy/16.png\"></li>\n                    <li><img src=\"https://minotar.net/avatar/_andy/16.png\"></li>\n                </ul>\n            </div>\n            <div class=\"col-sm-4 text-right\">\n                <div class=\"btn-group\" role=\"group\" aria-label=\"...\">\n                    <button type=\"button\" class=\"btn btn-default\">\n                        <span class=\"glyphicon glyphicon-thumbs-up\" aria-hidden=\"true\"></span> like\n                    </button>\n                    <button type=\"button\" class=\"btn btn-default\">\n                        <span class=\"glyphicon glyphicon-arrow-down\" aria-hidden=\"true\"></span> new post\n                    </button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\components\\forums\\forum-thread-announcement.vue"
  module.hot.dispose(function () {
    require("vueify-insert-css").cache["\n.cover {\n    background-size: cover;\n    background-repeat: no-repeat;\n    background-image: url('http://localhost/PCB_update/public/img/news_img.jpg');\n    height: 100px;\n}\n\n"] = false
    document.head.removeChild(__vueify_style__)
  })
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"vue":33,"vue-hot-reload-api":6,"vueify-insert-css":34});

loadjs.d("./resources/assets/js/routes/forums.thread.route.vue",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _forumThread = require('../components/forums/forum-thread.vue');

var _forumThread2 = _interopRequireDefault(_forumThread);

var _forumPost = require('../components/forums/forum-post.vue');

var _forumPost2 = _interopRequireDefault(_forumPost);

var _forumThreadActions = require('../components/forums/forum-thread-actions.vue');

var _forumThreadActions2 = _interopRequireDefault(_forumThreadActions);

var _forumPollForm = require('../components/forums/forum-poll-form.vue');

var _forumPollForm2 = _interopRequireDefault(_forumPollForm);

var _forumPollDisplay = require('../components/forums/forum-poll-display.vue');

var _forumPollDisplay2 = _interopRequireDefault(_forumPollDisplay);

var _forumErrorMixin = require('../mixins/forum-error.mixin.js');

var _forumErrorMixin2 = _interopRequireDefault(_forumErrorMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            thread: {
                root: {
                    posts: []
                }
            },
            users: [],
            blockError: '',

            showPostBox: true,
            showActionBar: true
        };
    },


    components: {
        'forum-thread': _forumThread2.default,
        'forum-post': _forumPost2.default,
        'thread-actions': _forumThreadActions2.default,
        'poll-form': _forumPollForm2.default,
        'poll-display': _forumPollDisplay2.default
    },

    mixins: [_forumErrorMixin2.default],

    computed: {
        isDeleted: function isDeleted() {
            return this.thread.deleted_at != null;
        },

        hasState: function hasState() {
            return this.thread.is_locked || this.thread.is_sealed || this.thread.is_archived || this.thread.deleted_at != null;
        }
    },

    methods: {
        getData: function getData() {
            this.$http.get('forums/thread/' + this.$route.params.topic).then(function (response) {
                this.thread = response.data.thread;

                // build user array with id as key
                this.users = [];
                var users = response.data.users;
                for (var i = 0; i < users.length; i++) {
                    var id = users[i].id;
                    this.users[id] = users[i];
                }
            }, function (response) {
                // handle 'no thread found' locally
                if (response.status == "404") {
                    this.blockError = 'no_thread';
                    this.showPostBox = false;
                    this.showActionBar = false;
                    return;
                }

                this.broadcastError(response);
            });
        },

        createPost: function createPost(id) {
            var editor = $('#post_editor');
            editor.summernote('disable');

            var postData = {
                thread_id: this.thread.id,
                parent_id: this.thread.root.id,
                parent_type: this.thread.root_type,
                message: editor.summernote('code')
            };

            var self = this;
            this.$http.post('forums/post/create', postData).then(function (response) {
                editor.summernote('enable');
                editor.summernote('reset');
                self.getData();
            }, function (response) {
                console.log(response);
                editor.summernote('enable');
            });
        },

        deleteThread: function deleteThread(isDelete) {
            var postData = {
                delete: isDelete
            };
            var self = this;
            this.$http.post('forums/thread/' + this.thread.id + '/delete', postData).then(function (response) {
                if (response.data.success) {
                    if (isDelete) {
                        $('#deleteModal').modal('hide');
                        this.$router.go('/forums/board/' + self.thread.board_id);
                    } else {
                        $('#restoreModal').modal('hide');
                    }
                    this.getData();
                }
            }, function (response) {
                console.log(response);
            });
        }
    },

    ready: function ready() {
        this.$dispatch('onTabSelected', 'forums');
        $('[data-toggle="tooltip"]').tooltip();

        $('#post_editor').summernote({
            height: 250,
            minHeight: 150,
            focus: true,
            placeholder: 'Write something here...'
        });
    },

    events: {
        'is_dirty': function is_dirty() {
            this.getData();
            return false;
        }
    },

    route: {
        activate: function activate() {
            this.getData();
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div>\n    <div class=\"modal fade\" id=\"deleteModal\" tabindex=\"-1\" role=\"dialog\">\n        <div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n                    <h4 class=\"modal-title\">Delete this thread?</h4>\n                </div>\n                <div class=\"modal-body\">\n                    <p>This thread will be moved to the recycling bin if you proceed.</p>\n                </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-danger\" v-on:click=\"deleteThread(true)\">Delete Thread</button>\n                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"modal fade\" id=\"restoreModal\" tabindex=\"-1\" role=\"dialog\">\n        <div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n                    <h4 class=\"modal-title\">Restore this thread?</h4>\n                </div>\n                <div class=\"modal-body\">\n                    <p>This thread will be moved back into public view if you proceed.</p>\n                </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-info\" v-on:click=\"deleteThread(false)\">Restore Thread</button>\n                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n    <div class=\"col-sm-12\">\n\n        <thread-actions v-if=\"showActionBar\" :thread_id=\"thread.id\" :is_stickied=\"thread.is_stickied\" :is_locked=\"thread.is_locked\" :is_sealed=\"thread.is_sealed\" :is_archived=\"thread.is_archived\">\n        </thread-actions>\n\n        <poll-form class=\"collapse\" id=\"pollForm\" :thread_id=\"thread.id\">\n        </poll-form>\n\n        <poll-display v-if=\"thread.poll != null\" :poll=\"thread.poll\">\n        </poll-display>\n\n        <div class=\"panel panel-default\" v-show=\"hasState\">\n            <ul class=\"list-group\">\n                <li class=\"list-group-item\" v-if=\"thread.is_locked\">\n                    <p></p><h4><span class=\"glyphicon glyphicon-lock\" aria-hidden=\"true\"></span> Topic Locked</h4><p></p>\n                    <p>This thread was locked at {{thread.last_action_at}}. No further posts or replies can be written.</p>\n                </li>\n                <li class=\"list-group-item\" v-if=\"thread.is_sealed\">\n                    <p></p><h4><span class=\"glyphicon glyphicon-lock\" aria-hidden=\"true\"></span> Topic Sealed</h4><p></p>\n                    <p>This thread was sealed at {{thread.last_action_at}}. Staff members included, no further posts or replies can be written.</p>\n                </li>\n                <li class=\"list-group-item\" v-if=\"thread.is_archived\">\n                    <p></p><h4><span class=\"glyphicon glyphicon-time\" aria-hidden=\"true\"></span> Topic Archived ({{thread.last_action_at}})</h4><p></p>\n                    <ul>\n                        <li>No posts / replies can be written</li>\n                        <li>No actions can be performed</li>\n                    </ul>\n                </li>\n                <li class=\"list-group-item\" v-if=\"isDeleted\">\n                    <p></p><h4>Topic Deleted</h4><p></p>\n                    <p>This thread was deleted at {{thread.last_action_at}}. No regular members can see this thread.</p>\n                    <hr>\n                    <button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#restoreModal\">\n                        <span class=\"glyphicon glyphicon-repeat\" aria-hidden=\"true\"></span> Restore Thread\n                    </button>\n                </li>\n            </ul>\n        </div>\n\n        <div class=\"panel panel-danger\" v-if=\"blockError == 'no_thread'\">\n            <div class=\"panel-heading\">\n                <h3>Thread Not Found</h3>\n            </div>\n            <div class=\"panel-body\">\n                The thread you are looking for does not exist. This could be because:\n                <ul>\n                    <li>the thread was just deleted</li>\n                    <li>an invalid URL</li>\n                    <li>permission to view this thread was revoked</li>\n                </ul>\n            </div>\n        </div>\n\n        <forum-thread :thread=\"thread\" :users=\"user\">\n        </forum-thread>\n\n        <forum-post v-for=\"reply in thread.root.posts\" :post=\"reply\" :users=\"users\" :index=\"$index\" :thread_id=\"thread.id\">\n        </forum-post>\n\n        <thread-actions v-if=\"showActionBar\" :thread_id=\"thread.id\" :is_stickied=\"thread.is_stickied\" :is_locked=\"thread.is_locked\" :is_sealed=\"thread.is_sealed\" :is_archived=\"thread.is_archived\">\n        </thread-actions>\n\n        <div class=\"panel panel-primary\" v-if=\"showPostBox\">\n            <div class=\"panel-heading\">\n                Write New Post\n            </div>\n\n            <div class=\"panel-body\">\n                <div id=\"post_editor\"></div>\n            </div>\n\n            <div class=\"panel-footer text-right\">\n                <button type=\"button\" class=\"btn btn-primary\" v-on:click=\"createPost\" id=\"btnCreate\" data-loading-text=\"Posting...\">\n                     <span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> Add Post\n                </button>\n            </div>\n        </div>\n\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\forums.thread.route.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"../components/forums/forum-poll-display.vue":37,"../components/forums/forum-poll-form.vue":38,"../components/forums/forum-post.vue":39,"../components/forums/forum-thread-actions.vue":40,"../components/forums/forum-thread.vue":43,"../mixins/forum-error.mixin.js":47,"vue":33,"vue-hot-reload-api":6});

loadjs.d("38",function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = require("babel-runtime/core-js/json/stringify");

var _stringify2 = _interopRequireDefault(_stringify);

var _forumErrorMixin = require("../../mixins/forum-error.mixin.js");

var _forumErrorMixin2 = _interopRequireDefault(_forumErrorMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            choices: [],
            placeholders: ["One whole sheep", "7 but they turned out to be pigs", "Wait, I was supposed to herd them?", "What's a sheep?", "I'm a sheep", "A lot of sheep", "Many sheep"],
            choiceIndex: 0,

            question: '',
            vote_req_post: false,
            votes_visible: false,
            anonymous_votes: false,
            max_choices: 1
        };
    },


    props: ['thread_id'],

    mixins: [_forumErrorMixin2.default],

    created: function created() {
        for (var i = 0; i < 3; i++) {
            this.addChoice();
        }
    },

    methods: {
        addChoice: function addChoice() {
            this.choices.push({
                placeholder: this.placeholders[this.choiceIndex],
                text: ''
            });

            this.choiceIndex = (this.choiceIndex + 1) % this.placeholders.length;
        },

        deleteChoice: function deleteChoice(index) {
            if (this.choices.length > 1) {
                this.choices.splice(index, 1);
            }
        },

        submit: function submit() {
            var choices = [];
            for (var i = 0; i < this.choices.length; i++) {
                choices.push(this.choices[i].text);
            }

            var data = {
                thread_id: this.thread_id,
                question: this.question,
                choices: (0, _stringify2.default)(choices),
                vote_req_post: this.vote_req_post,
                votes_visible: this.votes_visible,
                anonymous_votes: this.anonymous_votes
            };

            var self = this;
            this.$http.post('forums/poll/create', data).then(function (response) {
                console.log(response);
            }, function (response) {
                console.log(response);
                this.broadcastError(response);
            });
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<form method=\"post\" class=\"form-horizontal\" v-on:submit.prevent=\"submit\">\n    <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n           <h5>Add a Poll</h5>\n        </div>\n        <div class=\"panel-body\">\n            <div class=\"form-group\">\n                <label for=\"inputEmail3\" class=\"col-sm-2 control-label\">Question / Title</label>\n                <div class=\"col-sm-10\">\n                    <input type=\"text\" class=\"form-control input-lg\" placeholder=\"How many sheep are in your herd?\" v-model=\"question\">\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label for=\"inputEmail3\" class=\"col-sm-2 control-label\">Choices</label>\n                <div class=\"col-sm-10\">\n                    <div class=\"input-group\" v-for=\"choice in choices\">\n                        <span class=\"input-group-addon\" id=\"sizing-addon1\">{{$index + 1}}</span>\n                        <input type=\"text\" class=\"form-control\" v-model=\"choice.text\" placeholder=\"{{choice.placeholder}}\">\n                        <span class=\"input-group-btn\">\n                          <button class=\"btn btn-default\" type=\"button\" v-on:click=\"deleteChoice($index)\">X</button>\n                        </span>\n                    </div>\n\n                    <button class=\"btn btn-default\" style=\"margin-top: 5px\" type=\"button\" v-on:click=\"addChoice\">Add Choice</button>\n                </div>\n            </div>\n            <hr>\n            <div class=\"form-group\">\n                <label for=\"inputEmail3\" class=\"col-sm-2 control-label\">Vote Locks:</label>\n                <div class=\"col-sm-10\">\n                    <div class=\"radio\">\n                        <label>\n                            <input type=\"radio\" name=\"optionsRadios\" id=\"optionsRadios1\" value=\"option1\" checked=\"\">\n                            Never\n                        </label>\n                    </div>\n                    <div class=\"radio\">\n                        <label>\n                            <input type=\"radio\" name=\"optionsRadios\" id=\"optionsRadios2\" value=\"option2\">\n                            <input type=\"text\" class=\"form-control\" value=\"1\" placeholder=\"1\">\n                        </label>\n                    </div>\n                </div>\n            </div>\n            <hr>\n            <div class=\"form-group\">\n                <label for=\"inputEmail3\" class=\"col-sm-2 control-label\">Vote Settings</label>\n                <div class=\"col-sm-10\">\n                    <div class=\"checkbox\">\n                        <label>\n                            <input type=\"checkbox\" value=\"\" v-model=\"vote_req_post\">\n                            Vote must be accomponied by a message\n                        </label>\n                    </div>\n                    <div class=\"checkbox\">\n                        <label>\n                            <input type=\"checkbox\" value=\"\" v-model=\"votes_visible\">\n                            Vote results are always visible\n                        </label>\n                    </div>\n                    <div class=\"checkbox\">\n                        <label>\n                            <input type=\"checkbox\" value=\"\" v-model=\"anonymous_votes\">\n                            Votes are anonymous\n                        </label>\n                    </div>\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <label for=\"inputEmail3\" class=\"col-sm-2 control-label\">Max. Choices</label>\n                <div class=\"col-sm-1\">\n                    <input type=\"text\" class=\"form-control\" placeholder=\"1\" v-model=\"max_choices\">\n                </div>\n            </div>\n        </div>\n        <div class=\"panel-footer text-right\">\n            <input class=\"btn btn-primary\" type=\"submit\" value=\"Create Poll\">\n        </div>\n    </div>\n</form>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\components\\forums\\forum-poll-form.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"../../mixins/forum-error.mixin.js":47,"babel-runtime/core-js/json/stringify":1,"vue":33,"vue-hot-reload-api":6});

loadjs.d("1",function(require,module,exports){
module.exports = { "default": require("core-js/library/fn/json/stringify"), __esModule: true };
},{"core-js/library/fn/json/stringify":2});

loadjs.d("39",function(require,module,exports){
var __vueify_style__ = require("vueify-insert-css").insert("\nh3[_v-39d49db0] {\n    margin-top: 10px;\n}\n\n.username[_v-39d49db0] {\n    font-size: 1.4em;\n}\n\n.date[_v-39d49db0] {\n    color: #666;\n}\n\n.forum-post[_v-39d49db0] {\n    padding-top: 30px;\n    padding-bottom: 30px;\n}\n\ndt[_v-39d49db0], dd[_v-39d49db0] {\n    padding-bottom: 0;\n    margin-bottom: 0;\n}\n\n.user-stats[_v-39d49db0] {\n    font-size: 0.8em;\n    color: #666;\n    font-weight: normal;\n}\n\n.user-stats .glyphicon[_v-39d49db0] {\n    color: #df6745;\n    margin-right: 5px;\n}\n\n.user-stats div[_v-39d49db0] {\n    margin-bottom: 5px;\n}\n\n.liked-users li[_v-39d49db0] {\n    margin-right: 3px;\n    padding-right: 0;\n}\n")
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});


var moment = require('moment');

exports.default = {
    props: ['thread_id', 'post', 'users', 'index'],

    methods: {
        getTimeAgo: function getTimeAgo(time) {
            return moment(time).fromNow();
        },

        getAvatar: function getAvatar(user, size) {
            if (user.alias == null) return 'https://minotar.net/avatar/' + user.username + '/' + size + '.png';

            return 'https://minotar.net/avatar/' + user.alias + '/' + size + '.png';
        },

        toggleReplyEditor: function toggleReplyEditor(id) {
            var box = $('#replybox' + id);
            var editor = $('#replybox_editor' + id);

            if (box.attr('aria-expanded') == "false") {
                box.collapse('show');
                editor.summernote({
                    placeholder: 'Write your reply here... (Highlight text to see the toolbar)',
                    focus: true,
                    airMode: true
                });
            } else {
                box.collapse('hide');
                editor.summernote('destroy');
            }
        },

        createReply: function createReply(id, parent_id) {
            var editor = $('#replybox_editor' + id);
            editor.summernote('disable');

            var postData = {
                thread_id: this.thread_id,
                parent_id: parent_id,
                message: editor.summernote('code')
            };

            var self = this;
            this.$http.post('forums/post/create', postData).then(function (response) {
                editor.summernote('enable');
                editor.summernote('reset');
                self.$dispatch('is_dirty', true);
            }, function (response) {
                console.log(response);
                editor.summernote('enable');
            });
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div class=\"panel panel-default\" _v-39d49db0=\"\">\n    <div class=\"panel-body forum-post\" _v-39d49db0=\"\">\n\n        <div class=\"col-sm-2 text-center\" _v-39d49db0=\"\">\n            <span class=\"label label-success\" _v-39d49db0=\"\">Online</span>\n            <p _v-39d49db0=\"\">\n            <img v-bind:src=\"getAvatar(users[post.user_id], 64)\" alt=\"...\" class=\"img-rounded\" _v-39d49db0=\"\">\n            </p><p _v-39d49db0=\"\">\n            <span class=\"label label-default\" _v-39d49db0=\"\">Admin</span>\n            <span class=\"label label-default\" _v-39d49db0=\"\">Donator</span>\n\n            </p><hr _v-39d49db0=\"\">\n\n            <div class=\"user-stats\" _v-39d49db0=\"\">\n                <span class=\"pull-right\" _v-39d49db0=\"\">1,724</span>\n                <div class=\"text-left\" _v-39d49db0=\"\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\" _v-39d49db0=\"\"></span> Posts</div>\n\n                <span class=\"pull-right\" _v-39d49db0=\"\">438</span>\n                <div class=\"text-left\" _v-39d49db0=\"\"><span class=\"glyphicon glyphicon-file\" aria-hidden=\"true\" _v-39d49db0=\"\"></span> Topics</div>\n\n                <span class=\"pull-right\" _v-39d49db0=\"\">192</span>\n                <div class=\"text-left\" _v-39d49db0=\"\"><span class=\"glyphicon glyphicon-thumbs-up\" aria-hidden=\"true\" _v-39d49db0=\"\"></span> Thumbs</div>\n            </div>\n\n            \n            <p _v-39d49db0=\"\">\n            <span class=\"glyphicon glyphicon-text-background\" aria-hidden=\"true\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Former Names:\" _v-39d49db0=\"\"></span>\n            <span class=\" glyphicon glyphicon-king\" aria-hidden=\"true\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Events Won:\" _v-39d49db0=\"\"></span>\n        </p></div>\n        <div class=\"col-sm-10\" _v-39d49db0=\"\">\n            <div class=\"row\" _v-39d49db0=\"\">\n                <div class=\"col-sm-8\" _v-39d49db0=\"\">\n                    <p class=\"username\" _v-39d49db0=\"\"><a href=\"\" _v-39d49db0=\"\">{{users[post.user_id].username}}</a></p>\n                </div>\n                <div class=\"col-sm-4 text-right\" _v-39d49db0=\"\">\n                    <span class=\"date\" _v-39d49db0=\"\">{{getTimeAgo(post.created_at)}}</span>\n                </div>\n            </div>\n\n            {{{post.message}}}\n\n            <hr _v-39d49db0=\"\">\n\n            Signature\n        </div>\n\n    </div>\n\n    <ul class=\"list-group collapse\" id=\"replybox{{index}}\" aria-expanded=\"false\" _v-39d49db0=\"\">\n        <li class=\"list-group-item\" _v-39d49db0=\"\">\n            <div class=\"row\" _v-39d49db0=\"\">\n                <div class=\"col-xs-1 text-centered\" _v-39d49db0=\"\">\n                    <img src=\"https://minotar.net/avatar/_andy/32.png\" alt=\"...\" class=\"img-thumbnail\" _v-39d49db0=\"\">\n                </div>\n                <div class=\"col-xs-9\" _v-39d49db0=\"\">\n                    <div id=\"replybox_editor{{index}}\" _v-39d49db0=\"\"></div>\n                </div>\n                <div class=\"col-xs-2\" _v-39d49db0=\"\">\n                    <button type=\"button\" class=\"btn btn-primary btn-block\" id=\"btnCreate\" data-loading-text=\"Posting...\" v-on:click=\"createReply(index, post.id)\" _v-39d49db0=\"\">Post</button>\n                </div>\n            </div>\n        </li>\n    </ul>\n\n    <div class=\"panel-footer\" _v-39d49db0=\"\">\n        <div class=\"row\" _v-39d49db0=\"\">\n            <div class=\"col-sm-8\" _v-39d49db0=\"\">\n\n            </div>\n            <div class=\"col-sm-4 text-right\" _v-39d49db0=\"\">\n                <div class=\"btn-group\" role=\"group\" aria-label=\"...\" _v-39d49db0=\"\">\n                    <button type=\"button\" class=\"btn btn-default\" _v-39d49db0=\"\">\n                        <span class=\"glyphicon glyphicon-thumbs-up\" aria-hidden=\"true\" _v-39d49db0=\"\"></span> like\n                    </button>\n                    <button type=\"button\" class=\"btn btn-default\" v-on:click=\"toggleReplyEditor(index)\" _v-39d49db0=\"\">\n                        <span class=\"glyphicon glyphicon-arrow-left\" aria-hidden=\"true\" _v-39d49db0=\"\"></span> reply\n                    </button>\n                    <button type=\"button\" class=\"btn btn-default\" _v-39d49db0=\"\">\n                        <span class=\"glyphicon glyphicon-arrow-down\" aria-hidden=\"true\" _v-39d49db0=\"\"></span> new post\n                    </button>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <ul class=\"list-group\" _v-39d49db0=\"\">\n        <li class=\"list-group-item\" v-for=\"subreply in post.replies\" _v-39d49db0=\"\">\n\n            <div class=\"media\" _v-39d49db0=\"\">\n                <div class=\"media-left\" _v-39d49db0=\"\">\n                    <a href=\"#\" _v-39d49db0=\"\">\n                        <img class=\"media-object img-thumbnail\" v-bind:src=\"getAvatar(users[subreply.user_id], 32)\" alt=\"...\" _v-39d49db0=\"\">\n                    </a>\n                </div>\n                <div class=\"media-body\" _v-39d49db0=\"\">\n                    <h4 class=\"media-heading username\" _v-39d49db0=\"\"><a href=\"\" _v-39d49db0=\"\">{{users[subreply.user_id].username}}</a> - {{getTimeAgo(subreply.created_at)}}</h4>\n                    {{{subreply.message}}}\n                </div>\n            </div>\n        </li>\n    </ul>\n\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\components\\forums\\forum-post.vue"
  module.hot.dispose(function () {
    require("vueify-insert-css").cache["\nh3[_v-39d49db0] {\n    margin-top: 10px;\n}\n\n.username[_v-39d49db0] {\n    font-size: 1.4em;\n}\n\n.date[_v-39d49db0] {\n    color: #666;\n}\n\n.forum-post[_v-39d49db0] {\n    padding-top: 30px;\n    padding-bottom: 30px;\n}\n\ndt[_v-39d49db0], dd[_v-39d49db0] {\n    padding-bottom: 0;\n    margin-bottom: 0;\n}\n\n.user-stats[_v-39d49db0] {\n    font-size: 0.8em;\n    color: #666;\n    font-weight: normal;\n}\n\n.user-stats .glyphicon[_v-39d49db0] {\n    color: #df6745;\n    margin-right: 5px;\n}\n\n.user-stats div[_v-39d49db0] {\n    margin-bottom: 5px;\n}\n\n.liked-users li[_v-39d49db0] {\n    margin-right: 3px;\n    padding-right: 0;\n}\n"] = false
    document.head.removeChild(__vueify_style__)
  })
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"moment":4,"vue":33,"vue-hot-reload-api":6,"vueify-insert-css":34});

loadjs.d("3",function(require,module,exports){
var core = module.exports = {version: '1.2.6'};
if(typeof __e == 'number')__e = core; // eslint-disable-line no-undef
},{});

loadjs.d("35",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    props: ['queue']
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<template v-for=\"item in queue\">\n    <div class=\"alert alert-danger alert-dismissible\" role=\"alert\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n        <h3>{{item.code}} Error</h3>\n        The web server returned an error: <strong>{{item.message}}</strong>\n    </div>\n</template>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\components\\forums\\forum-alerts.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"vue":33,"vue-hot-reload-api":6});

loadjs.d("36",function(require,module,exports){
var __vueify_style__ = require("vueify-insert-css").insert("\n$chartColours: #df6745, #f76540, #ffcb05, #62cd9f, #b67baa;\n\na[_v-4497670e] {\n    color: #2d70c9;\n}\n\n.board-desc[_v-4497670e]\n{\n    color: #666;\n    font-size: 0.85em;\n    letter-spacing: normal;\n}\n\n.table-boards tbody tr[_v-4497670e]\n{\n    margin-bottom: 2px;\n    background: #fff;\n}\n\n.table-boards tbody tr td[_v-4497670e]\n{\n    padding: 22px 15px;\n}\n\n.table-boards h5[_v-4497670e]\n{\n    margin: 0 0 5px 0;\n    font-size: 1.4em;\n}\n\n.table-boards .board-icon[_v-4497670e]\n{\n    width: 75px;\n    height: 75px;\n}\n\n.table-boards .board-stats[_v-4497670e]\n{\n    width: 12%;\n    text-align: center;\n}\n\n.table-boards .board-stats strong[_v-4497670e]\n{\n    margin: 0 0 5px 0;\n    font-size: 1.2em;\n    line-height: 1.1;\n    display: block;\n}\n\n.table-boards .board-post[_v-4497670e]\n{\n    width: 30%;\n    font-size: 0.8em;\n}\n\n.table-boards .board-post > div[_v-4497670e]\n{\n    display: inline-block;\n    vertical-align: top;\n}\n\n.table-boards .board-post > [_v-4497670e]:first-child\n{\n    margin-right: 10px;\n    padding-top: 5px;\n}\n\n.table-boards .board-child[_v-4497670e]\n{\n    background: #fff;\n}\n\n.table-boards .board-child td[_v-4497670e]\n{\n    padding-top: 5px;\n    padding-bottom: 5px;\n}\n\n.circle[_v-4497670e]\n{\n    margin-left: auto;\n    margin-right: auto;\n    border-radius: 50%;\n    width: 100%;\n    position: relative;\n}\n\n.circle[_v-4497670e]:before\n{\n    content: \"\";\n    display: block;\n    padding-top: 100%;\n}\n\n.circle-inner[_v-4497670e]\n{\n    position: absolute;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    right: 0;\n    text-align: center;\n}\n\n.circle-text[_v-4497670e]\n{\n    margin: auto;\n    position: absolute;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    right: 0;\n    height: 1em;\n    line-height: 1em;\n    font-size: 1em;\n    color: #fff;\n}\n\n.circle[_v-4497670e]:nth-child(3n+1)\n{\n    background: #56c7fa;\n}\n\n.circle[_v-4497670e]:nth-child(3n+2)\n{\n    background: #df6745;\n}\n\n.circle[_v-4497670e]:nth-child(3n+3)\n{\n    background: #ffcb05;\n}\n")
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});


var moment = require('moment');

exports.default = {
    props: ['name', 'boards'],

    methods: {
        getTimeAgo: function getTimeAgo(time) {
            return moment(time).fromNow();
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<table class=\"table table-boards\" _v-4497670e=\"\">\n    <thead _v-4497670e=\"\">\n        <tr _v-4497670e=\"\">\n            <th colspan=\"5\" _v-4497670e=\"\">{{name}}</th>\n        </tr>\n    </thead>\n    <tbody _v-4497670e=\"\">\n\n        <template v-for=\"board in boards\">\n            <tr _v-4497670e=\"\">\n                <td class=\"board-icon\" _v-4497670e=\"\">\n                  <div class=\"circle\" _v-4497670e=\"\">\n                       <div class=\"circle-inner\" _v-4497670e=\"\">\n                           <div class=\"circle-text\" _v-4497670e=\"\">\n                              <span class=\"glyphicon glyphicon-tower\" aria-hidden=\"true\" _v-4497670e=\"\"></span>\n                            </div>\n                       </div>\n                  </div>\n                </td>\n                <td class=\"board-name\" _v-4497670e=\"\">\n                    <h5 _v-4497670e=\"\"><a href=\"#\" v-link=\"{path: '/forums/board/' + board.id }\" _v-4497670e=\"\">{{board.name}}</a></h5>\n                    <span class=\"board-desc\" _v-4497670e=\"\">{{board.description}}</span>\n                </td>\n                <td class=\"board-stats\" _v-4497670e=\"\">\n                    <strong _v-4497670e=\"\">{{board.total_threads}}</strong>\n                    <span class=\"board-desc\" _v-4497670e=\"\">Topics</span>\n                </td>\n                <td class=\"board-stats\" _v-4497670e=\"\">\n                    <strong _v-4497670e=\"\">{{board.total_posts}}</strong>\n                    <span class=\"board-desc\" _v-4497670e=\"\">Posts</span>\n                </td>\n                <td class=\"board-post\" _v-4497670e=\"\">\n                    <template v-if=\"board.latest_thread || board.latest_post\">\n                        <template v-if=\"board.latest_post\">\n                            <div _v-4497670e=\"\">\n                                <img src=\"https://minotar.net/avatar/_andy/32.png\" _v-4497670e=\"\">\n                            </div>\n                            <div _v-4497670e=\"\">\n                                Last post by <a href=\"#\" v-link=\"{path:'/user/' + board.latest_post.user_id}\" _v-4497670e=\"\">someone</a><br _v-4497670e=\"\">\n                                in <a href=\"\" v-link=\"{path:'/forums/topic/' + board.latest_thread.id}\" _v-4497670e=\"\">{{board.latest_thread.title}}</a><br _v-4497670e=\"\">\n                                {{getTimeAgo(board.latest_post.created_at)}}\n                            </div>\n                        </template>\n                        <template v-else=\"\">\n                            <div _v-4497670e=\"\">\n                                <img src=\"https://minotar.net/avatar/_andy/32.png\" _v-4497670e=\"\">\n                            </div>\n                            <div _v-4497670e=\"\">\n                                Last post by <a href=\"#\" v-link=\"{path:'/user/' + board.latest_thread.root.user_id}\" _v-4497670e=\"\">someone</a><br _v-4497670e=\"\">\n                                in <a href=\"\" v-link=\"{path:'/forums/topic/' + board.latest_thread.id}\" _v-4497670e=\"\">{{board.latest_thread.title}}</a><br _v-4497670e=\"\">\n                                {{getTimeAgo(board.latest_thread.last_post_at)}}\n                            </div>\n                        </template>\n                    </template>\n                    <template v-else=\"\">\n                        No Post Yet\n                    </template>\n                </td>\n            </tr>\n\n            <tr class=\"board-child\" v-for=\"child in board.child_boards\" _v-4497670e=\"\">\n                <td _v-4497670e=\"\">\n\n                </td>\n                <td _v-4497670e=\"\">\n                    <h6 _v-4497670e=\"\"><a href=\"#\" v-link=\"{path: '/forums/board/' + child.id }\" _v-4497670e=\"\">Child board</a></h6>\n                </td>\n                <td _v-4497670e=\"\">\n                    <strong _v-4497670e=\"\">{{child.total_threads}}</strong>\n                </td>\n                <td _v-4497670e=\"\">\n                    <strong _v-4497670e=\"\">{{child.total_posts}}</strong>\n                </td>\n                <td _v-4497670e=\"\">\n\n                </td>\n            </tr>\n        </template>\n\n    </tbody>\n</table>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\components\\forums\\forum-category.vue"
  module.hot.dispose(function () {
    require("vueify-insert-css").cache["\n$chartColours: #df6745, #f76540, #ffcb05, #62cd9f, #b67baa;\n\na[_v-4497670e] {\n    color: #2d70c9;\n}\n\n.board-desc[_v-4497670e]\n{\n    color: #666;\n    font-size: 0.85em;\n    letter-spacing: normal;\n}\n\n.table-boards tbody tr[_v-4497670e]\n{\n    margin-bottom: 2px;\n    background: #fff;\n}\n\n.table-boards tbody tr td[_v-4497670e]\n{\n    padding: 22px 15px;\n}\n\n.table-boards h5[_v-4497670e]\n{\n    margin: 0 0 5px 0;\n    font-size: 1.4em;\n}\n\n.table-boards .board-icon[_v-4497670e]\n{\n    width: 75px;\n    height: 75px;\n}\n\n.table-boards .board-stats[_v-4497670e]\n{\n    width: 12%;\n    text-align: center;\n}\n\n.table-boards .board-stats strong[_v-4497670e]\n{\n    margin: 0 0 5px 0;\n    font-size: 1.2em;\n    line-height: 1.1;\n    display: block;\n}\n\n.table-boards .board-post[_v-4497670e]\n{\n    width: 30%;\n    font-size: 0.8em;\n}\n\n.table-boards .board-post > div[_v-4497670e]\n{\n    display: inline-block;\n    vertical-align: top;\n}\n\n.table-boards .board-post > [_v-4497670e]:first-child\n{\n    margin-right: 10px;\n    padding-top: 5px;\n}\n\n.table-boards .board-child[_v-4497670e]\n{\n    background: #fff;\n}\n\n.table-boards .board-child td[_v-4497670e]\n{\n    padding-top: 5px;\n    padding-bottom: 5px;\n}\n\n.circle[_v-4497670e]\n{\n    margin-left: auto;\n    margin-right: auto;\n    border-radius: 50%;\n    width: 100%;\n    position: relative;\n}\n\n.circle[_v-4497670e]:before\n{\n    content: \"\";\n    display: block;\n    padding-top: 100%;\n}\n\n.circle-inner[_v-4497670e]\n{\n    position: absolute;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    right: 0;\n    text-align: center;\n}\n\n.circle-text[_v-4497670e]\n{\n    margin: auto;\n    position: absolute;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    right: 0;\n    height: 1em;\n    line-height: 1em;\n    font-size: 1em;\n    color: #fff;\n}\n\n.circle[_v-4497670e]:nth-child(3n+1)\n{\n    background: #56c7fa;\n}\n\n.circle[_v-4497670e]:nth-child(3n+2)\n{\n    background: #df6745;\n}\n\n.circle[_v-4497670e]:nth-child(3n+3)\n{\n    background: #ffcb05;\n}\n"] = false
    document.head.removeChild(__vueify_style__)
  })
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"moment":4,"vue":33,"vue-hot-reload-api":6,"vueify-insert-css":34});

loadjs.d("37",function(require,module,exports){
var __vueify_style__ = require("vueify-insert-css").insert("\n.ct-series-a .ct-bar {\n    stroke: #df6745;\n    stroke-width: 20px;\n    /*stroke-linecap: round;*/\n}\n\n.progress {\n    margin: 0;\n}\n")
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _forumErrorMixin = require('../../mixins/forum-error.mixin.js');

var _forumErrorMixin2 = _interopRequireDefault(_forumErrorMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            tab: 'vote',
            pollSelection: []
        };
    },


    props: ['poll'],

    mixins: [_forumErrorMixin2.default],

    computed: {
        totalVotes: function totalVotes() {
            var total = 0;
            for (var i = 0; i < this.poll.choices.length; i++) {
                total += this.poll.choices[i].votes.length;
            }
            return total;
        }
    },

    methods: {
        setTab: function setTab(tab) {
            this.tab = tab;
        },

        submitVote: function submitVote() {
            var data = {
                selection: this.pollSelection
            };

            var self = this;
            this.$http.post('forums/poll/' + this.poll.id + '/vote', data).then(function (response) {
                console.log(response);
            }, function (response) {
                console.log(response);
                this.broadcastError(response);
            });
        }
    }

};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<form action=\"post\" v-on:submit.prevent=\"submitVote\">\n    <div class=\"panel panel-default\">\n        <div class=\"panel-heading\">\n            <div class=\"row\">\n                <div class=\"col-sm-8\">\n                    <h3>Poll: {{poll.question}}</h3>\n                </div>\n                <div class=\"col-sm-4\">\n                    <ul class=\"nav nav-pills nav-justified\">\n                        <li role=\"presentation\" v-bind:class=\"{active: tab == 'vote'}\"><a href=\"\" v-on:click.prevent=\"setTab('vote')\">Vote</a></li>\n                        <li role=\"presentation\" v-bind:class=\"{active: tab == 'results'}\"><a href=\"\" v-on:click.prevent=\"setTab('results')\">Results</a></li>\n                    </ul>\n                </div>\n            </div>\n        </div>\n        <div v-show=\"tab == 'vote'\">\n            <div class=\"panel-body\">\n                <div class=\"radio\" v-for=\"choice in poll.choices\">\n                    <label>\n                        <input type=\"radio\" v-model=\"pollSelection\" id=\"selection{{$index}}\" value=\"{{choice.id}}\">\n                        {{choice.choice}}\n                    </label>\n                    <hr>\n                </div>\n            </div>\n            <div class=\"panel-footer text-right\">\n                <button class=\"btn btn-lg btn-primary\" type=\"submit\">Submit Vote</button>\n            </div>\n        </div>\n        <div v-show=\"tab == 'results'\">\n            <div class=\"panel-body\">\n                <template v-for=\"choice in poll.choices\">\n                <div class=\"row\">\n                    <div class=\"col-sm-3 text-right\">\n                        {{choice.choice}}\n                    </div>\n                    <div class=\"col-sm-5\">\n                        <div class=\"progress\">\n                            <div class=\"progress-bar\" role=\"progressbar\" v-bind:style=\"{width: (choice.votes.length / totalVotes) * 100 + '%'}\">\n                                {{(choice.votes.length / totalVotes) * 100}}%\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col-sm-4\">\n                        <ul class=\"list-inline liked-users\">\n                            <li>{{choice.votes.length}} Votes</li>\n                            <li v-for=\"voter in choice.votes\">\n                                <img src=\"https://minotar.net/avatar/_andy/16.png\" class=\"img-circle\">\n                            </li>\n                        </ul>\n                    </div>\n                </div>\n                <div class=\"row\"><hr></div>\n                </template>\n            </div>\n        </div>\n\n    </div>\n</form>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\components\\forums\\forum-poll-display.vue"
  module.hot.dispose(function () {
    require("vueify-insert-css").cache["\n.ct-series-a .ct-bar {\n    stroke: #df6745;\n    stroke-width: 20px;\n    /*stroke-linecap: round;*/\n}\n\n.progress {\n    margin: 0;\n}\n"] = false
    document.head.removeChild(__vueify_style__)
  })
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"../../mixins/forum-error.mixin.js":47,"vue":33,"vue-hot-reload-api":6,"vueify-insert-css":34});

loadjs.d("40",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _forumErrorMixin = require('../../mixins/forum-error.mixin.js');

var _forumErrorMixin2 = _interopRequireDefault(_forumErrorMixin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {

    props: ['thread_id', 'is_stickied', 'is_locked', 'is_sealed', 'is_archived'],

    mixins: [_forumErrorMixin2.default],

    methods: {
        setState: function setState(state) {
            var self = this;
            this.$http.post('forums/thread/' + this.thread_id + '/' + state, {}).then(function (response) {
                if (response.data.success) {
                    self.$dispatch('is_dirty', true);
                }
            }, function (response) {
                this.broadcastError(response);
            });
        },

        setUnread: function setUnread() {
            this.$http.post('forums/thread/' + this.thread_id + '/unread', {}).then(function (response) {
                if (response.data.success) {}
            }, function (response) {
                this.broadcastError(response);
            });
        }
    }

};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div class=\"panel panel-default\">\n    <div class=\"panel-body text-right\">\n        <div class=\"btn-group\" role=\"group\" aria-label=\"...\">\n            <button type=\"button\" class=\"btn btn-default\" data-toggle=\"collapse\" href=\"#pollForm\">\n                <span class=\"glyphicon glyphicon-stats\" aria-hidden=\"true\"></span> Add a Poll\n            </button>\n        </div>\n        <div class=\"btn-group\" role=\"group\" aria-label=\"...\">\n            <button type=\"button\" class=\"btn btn-default\" v-on:click=\"setUnread\">\n                <span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\"></span> Mark as Unread\n            </button>\n        </div>\n        <div class=\"btn-group\" role=\"group\" aria-label=\"...\">\n            <button type=\"button\" class=\"btn btn-default\" v-on:click=\"setState('sticky')\" v-bind:class=\"{'active': is_stickied}\">\n                <span class=\"glyphicon glyphicon-pushpin\" aria-hidden=\"true\"></span> <span v-if=\"is_stickied\">Un</span>Pin\n            </button>\n            <button type=\"button\" class=\"btn btn-default\" v-on:click=\"setState('lock')\" v-bind:class=\"{'active': is_locked}\">\n                <span class=\"glyphicon glyphicon-lock\" aria-hidden=\"true\"></span> <span v-if=\"is_locked\">Un</span>Lock\n            </button>\n            <button type=\"button\" class=\"btn btn-default\" v-on:click=\"setState('seal')\" v-bind:class=\"{'active': is_sealed}\">\n                <span class=\"glyphicon glyphicon-lock\" aria-hidden=\"true\"></span> <span v-if=\"is_sealed\">Un</span>Seal\n            </button>\n            <button type=\"button\" class=\"btn btn-default\" v-on:click=\"setState('archive')\" v-bind:class=\"{'active': is_archived}\">\n                <span class=\"glyphicon glyphicon-time\" aria-hidden=\"true\"></span> <span v-if=\"is_archived\">Un</span>Archive\n            </button>\n        </div>\n        <div class=\"btn-group\" role=\"group\" aria-label=\"...\">\n            <button type=\"button\" class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#deleteModal\">\n                <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span> Delete\n            </button>\n            <button type=\"button\" class=\"btn btn-danger dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                <span class=\"caret\"></span>\n                <span class=\"sr-only\">Toggle Dropdown</span>\n            </button>\n            <ul class=\"dropdown-menu\">\n                <li><a href=\"#\">Delete and Warn</a></li>\n                <li><a href=\"#\">Delete and Ban</a></li>\n            </ul>\n        </div>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\components\\forums\\forum-thread-actions.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"../../mixins/forum-error.mixin.js":47,"vue":33,"vue-hot-reload-api":6});

loadjs.d("47",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    methods: {
        // bubble upwards an AJAX response error
        broadcastError: function broadcastError(response) {
            this.$dispatch('forum-error', response);
        }
    }
};

},{});

loadjs.d("43",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _forumThreadTopic = require('./forum-thread-topic.vue');

var _forumThreadTopic2 = _interopRequireDefault(_forumThreadTopic);

var _forumThreadAnnouncement = require('./forum-thread-announcement.vue');

var _forumThreadAnnouncement2 = _interopRequireDefault(_forumThreadAnnouncement);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    props: ['thread', 'users'],

    components: {
        'forum-topic': _forumThreadTopic2.default,
        'forum-announcement': _forumThreadAnnouncement2.default
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<forum-topic v-if=\"thread.root_type == 'post'\" :thread=\"thread\" :root=\"thread.root\">\n</forum-topic>\n\n<forum-announcement v-if=\"thread.root_type == 'announcement'\" :thread=\"thread\" :root=\"thread.root\">\n</forum-announcement>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\components\\forums\\forum-thread.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"./forum-thread-announcement.vue":41,"./forum-thread-topic.vue":42,"vue":33,"vue-hot-reload-api":6});

loadjs.d("42",function(require,module,exports){
var __vueify_style__ = require("vueify-insert-css").insert("\nh3[_v-366d66bc] {\n    margin-top: 10px;\n}\n\n.username[_v-366d66bc] {\n    font-size: 1.4em;\n}\n\n.date[_v-366d66bc] {\n    color: #666;\n}\n\n.forum-post[_v-366d66bc] {\n    padding-top: 30px;\n    padding-bottom: 30px;\n}\n\ndt[_v-366d66bc], dd[_v-366d66bc] {\n    padding-bottom: 0;\n    margin-bottom: 0;\n}\n\n.user-stats[_v-366d66bc] {\n    font-size: 0.8em;\n    color: #666;\n    font-weight: normal;\n}\n\n.user-stats .glyphicon[_v-366d66bc] {\n    color: #df6745;\n    margin-right: 5px;\n}\n\n.user-stats div[_v-366d66bc] {\n    margin-bottom: 5px;\n}\n\n.liked-users li[_v-366d66bc] {\n    margin-right: 3px;\n    padding-right: 0;\n}\n")
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});


var moment = require('moment');

exports.default = {
    props: ['thread', 'root'],

    methods: {
        getTimeAgo: function getTimeAgo(time) {
            return moment(time).fromNow();
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div class=\"panel panel-info\" _v-366d66bc=\"\">\n    <div class=\"panel-heading\" _v-366d66bc=\"\">\n        <div class=\"row\" _v-366d66bc=\"\">\n            <div class=\"col-sm-8\" _v-366d66bc=\"\">\n                <h3 _v-366d66bc=\"\"><span class=\"glyphicon glyphicon-pushpin\" v-if=\"thread.is_stickied\" aria-hidden=\"true\" _v-366d66bc=\"\"></span> {{thread.title}}</h3>\n            </div>\n            <div class=\"col-sm-4 text-right\" _v-366d66bc=\"\">\n                <ul class=\"list-inline text-center\" _v-366d66bc=\"\">\n                    <li _v-366d66bc=\"\">\n                        <dt _v-366d66bc=\"\">{{thread.reply_count}}</dt>\n                        <dd _v-366d66bc=\"\">Replies\n                    </dd></li>\n                    <li _v-366d66bc=\"\">\n                        <dt _v-366d66bc=\"\">17</dt>\n                        <dd _v-366d66bc=\"\">Posters\n                    </dd></li>\n                    <li _v-366d66bc=\"\">\n                        <dt _v-366d66bc=\"\">{{thread.view_count}}</dt>\n                        <dd _v-366d66bc=\"\">Views\n                    </dd></li>\n                </ul>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"panel-body forum-post\" _v-366d66bc=\"\">\n        <div class=\"col-sm-2 text-center\" _v-366d66bc=\"\">\n            <span class=\"label label-success\" _v-366d66bc=\"\">Online</span>\n            <p _v-366d66bc=\"\">\n            <img src=\"https://minotar.net/avatar/_andy/64.png\" alt=\"...\" class=\"img-rounded\" _v-366d66bc=\"\">\n            </p><p _v-366d66bc=\"\">\n            <span class=\"label label-default\" _v-366d66bc=\"\">Admin</span>\n            <span class=\"label label-default\" _v-366d66bc=\"\">Donator</span>\n\n            </p><hr _v-366d66bc=\"\">\n\n            <div class=\"user-stats\" _v-366d66bc=\"\">\n                <span class=\"pull-right\" _v-366d66bc=\"\">1,724</span>\n                <div class=\"text-left\" _v-366d66bc=\"\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\" _v-366d66bc=\"\"></span> Posts</div>\n\n                <span class=\"pull-right\" _v-366d66bc=\"\">438</span>\n                <div class=\"text-left\" _v-366d66bc=\"\"><span class=\"glyphicon glyphicon-file\" aria-hidden=\"true\" _v-366d66bc=\"\"></span> Topics</div>\n\n                <span class=\"pull-right\" _v-366d66bc=\"\">192</span>\n                <div class=\"text-left\" _v-366d66bc=\"\"><span class=\"glyphicon glyphicon-thumbs-up\" aria-hidden=\"true\" _v-366d66bc=\"\"></span> Thumbs</div>\n            </div>\n\n            \n            <p _v-366d66bc=\"\">\n            <span class=\"glyphicon glyphicon-text-background\" aria-hidden=\"true\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Former Names:\" _v-366d66bc=\"\"></span>\n            <span class=\" glyphicon glyphicon-king\" aria-hidden=\"true\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Events Won:\" _v-366d66bc=\"\"></span>\n        </p></div>\n        <div class=\"col-sm-10\" _v-366d66bc=\"\">\n            <div class=\"row\" _v-366d66bc=\"\">\n                <div class=\"col-sm-8\" _v-366d66bc=\"\">\n                    <p class=\"username\" _v-366d66bc=\"\"><a href=\"#\" v-link=\"{path: '/user/' + root.user_id}\" _v-366d66bc=\"\">Andy</a></p>\n                </div>\n                <div class=\"col-sm-4 text-right\" _v-366d66bc=\"\">\n                    <span class=\"date\" _v-366d66bc=\"\">{{getTimeAgo(root.created_at)}}</span>\n                </div>\n            </div>\n\n            {{{root.message}}}\n\n            <hr _v-366d66bc=\"\">\n\n            Signature\n        </div>\n    </div>\n    <div class=\"panel-footer\" _v-366d66bc=\"\">\n        <div class=\"row\" _v-366d66bc=\"\">\n            <div class=\"col-sm-8\" _v-366d66bc=\"\">\n                <ul class=\"list-inline liked-users\" _v-366d66bc=\"\">\n                    <li _v-366d66bc=\"\">Liked by 17 users</li>\n                    <li _v-366d66bc=\"\"><img src=\"https://minotar.net/avatar/_andy/16.png\" class=\"img-circle\" _v-366d66bc=\"\"></li>\n                    <li _v-366d66bc=\"\"><img src=\"https://minotar.net/avatar/_andy/16.png\" class=\"img-circle\" _v-366d66bc=\"\"></li>\n                    <li _v-366d66bc=\"\"><img src=\"https://minotar.net/avatar/_andy/16.png\" class=\"img-circle\" _v-366d66bc=\"\"></li>\n                    <li _v-366d66bc=\"\"><img src=\"https://minotar.net/avatar/_andy/16.png\" class=\"img-circle\" _v-366d66bc=\"\"></li>\n                    <li _v-366d66bc=\"\"><img src=\"https://minotar.net/avatar/_andy/16.png\" class=\"img-circle\" _v-366d66bc=\"\"></li>\n                </ul>\n            </div>\n            <div class=\"col-sm-4 text-right\" _v-366d66bc=\"\">\n                <div class=\"btn-group\" role=\"group\" aria-label=\"...\" _v-366d66bc=\"\">\n                    <button type=\"button\" class=\"btn btn-default\" _v-366d66bc=\"\">\n                        <span class=\"glyphicon glyphicon-thumbs-up\" aria-hidden=\"true\" _v-366d66bc=\"\"></span> like\n                    </button>\n                    <button type=\"button\" class=\"btn btn-default\" _v-366d66bc=\"\">\n                        <span class=\"glyphicon glyphicon-arrow-down\" aria-hidden=\"true\" _v-366d66bc=\"\"></span> new post\n                    </button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\components\\forums\\forum-thread-topic.vue"
  module.hot.dispose(function () {
    require("vueify-insert-css").cache["\nh3[_v-366d66bc] {\n    margin-top: 10px;\n}\n\n.username[_v-366d66bc] {\n    font-size: 1.4em;\n}\n\n.date[_v-366d66bc] {\n    color: #666;\n}\n\n.forum-post[_v-366d66bc] {\n    padding-top: 30px;\n    padding-bottom: 30px;\n}\n\ndt[_v-366d66bc], dd[_v-366d66bc] {\n    padding-bottom: 0;\n    margin-bottom: 0;\n}\n\n.user-stats[_v-366d66bc] {\n    font-size: 0.8em;\n    color: #666;\n    font-weight: normal;\n}\n\n.user-stats .glyphicon[_v-366d66bc] {\n    color: #df6745;\n    margin-right: 5px;\n}\n\n.user-stats div[_v-366d66bc] {\n    margin-bottom: 5px;\n}\n\n.liked-users li[_v-366d66bc] {\n    margin-right: 3px;\n    padding-right: 0;\n}\n"] = false
    document.head.removeChild(__vueify_style__)
  })
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"moment":4,"vue":33,"vue-hot-reload-api":6,"vueify-insert-css":34});

loadjs.d("2",function(require,module,exports){
var core = require('../../modules/$.core');
module.exports = function stringify(it){ // eslint-disable-line no-unused-vars
  return (core.JSON && core.JSON.stringify || JSON.stringify).apply(JSON, arguments);
};
},{"../../modules/$.core":3});
