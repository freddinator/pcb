
loadjs.d("./resources/assets/js/routes/staff/admin.forum.boards.new.category.route.vue",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    data: function data() {
        return {
            name: '',
            position: -1,
            categories: [],
            category: [],

            isEdit: false
        };
    },


    methods: {
        getCategories: function getCategories() {
            this.$http.get('forums/category').then(function (response) {
                this.categories = response.data.forum_categories;

                for (var i = 0; i < this.categories.length; i++) {
                    this.categories[i].placeholder = "Before '" + this.categories[i].name + "'";
                }
                this.categories.push({
                    order: this.categories.length + 1,
                    placeholder: "Last"
                });

                // if editing, for consistency remove an option from the 'position' list
                if (this.isEdit) {
                    var delIndex = -1;
                    for (var i = 0; i < this.categories.length; i++) {
                        if (this.categories[i].id == this.category.id) {
                            delIndex = i;
                            break;
                        }
                    }
                    this.categories.splice(delIndex, 1);
                    this.position = this.category.order;
                } else {
                    this.position = this.categories[0].order;
                }
            }, function (response) {
                console.log(response);
            });
        },

        getCategory: function getCategory(id, callback) {
            this.$http.get('forums/category/' + id).then(function (response) {
                this.category = response.data.forum_category;
                this.name = this.category.name;
                this.position = this.category.order;

                callback();
            }, function (response) {
                console.log(response);
            });
        },

        saveCategory: function saveCategory() {
            var data = {
                name: this.name,
                position: this.position
            };
            var url = this.isEdit ? 'forums/category/' + this.$route.params.id + '/save' : 'forums/category/create';
            this.$http.post(url, data).then(function (response) {
                this.$router.go('/staff/forum/boards');
            }, function (response) {
                console.log(response);
            });
        },

        deleteCategory: function deleteCategory() {
            this.$http.post('forums/category/' + this.$route.params.id + '/delete', {}).then(function (response) {
                $('#deleteModal').modal('hide');
                this.$router.go('/staff/forum/boards');
            }, function (response) {
                console.log(response);
            });
        }
    },

    route: {
        activate: function activate() {
            var self = this;
            var id = this.$route.params.id;
            if (typeof id !== 'undefined') {
                this.isEdit = true;
                this.getCategory(id, function () {
                    self.getCategories();
                });
            } else {
                this.getCategories();
            }
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div>\n    <div class=\"modal fade\" id=\"deleteModal\" tabindex=\"-1\" role=\"dialog\" v-if=\"isEdit\">\n        <div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n                    <h4 class=\"modal-title\">Delete this category?</h4>\n                </div>\n                <div class=\"modal-body\">\n                    <p>This cannot be undone. Any existing boards within the category will not be visible until they are reassigned to a new category manually.</p>\n                </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-danger\" v-on:click=\"deleteCategory\">Delete Category</button>\n                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <h3 v-if=\"!isEdit\">New Category</h3>\n    <h3 v-if=\"isEdit\">Edit Category: {{category.name}}</h3>\n    <hr>\n    <form v-on:submit.prevent=\"saveCategory()\">\n        <div class=\"form-group\">\n            <label>Category Name</label>\n            <input type=\"text\" v-model=\"name\" class=\"form-control\" placeholder=\"Name\">\n        </div>\n        <div class=\"form-group\">\n            <label>Position</label>\n            <select class=\"form-control\" v-model=\"position\">\n                <template v-for=\"category in categories\">\n                    <option value=\"{{category.order}}\">{{category.placeholder}}</option>\n                </template>\n            </select> {{position}}\n        </div>\n        <div class=\"form-group\">\n            <label>Viewable By</label>\n            <div class=\"checkbox\">\n                <label>\n                    <input type=\"checkbox\" value=\"\">\n                    Option one is this and that—be sure to include why it's great\n                </label>\n            </div>\n            <div class=\"checkbox disabled\">\n                <label>\n                    <input type=\"checkbox\" value=\"\" disabled=\"\">\n                    Option two is disabled\n                </label>\n            </div>\n        </div>\n        <button type=\"submit\" class=\"btn btn-primary\" v-if=\"!isEdit\">Create Category</button>\n        <template v-if=\"isEdit\">\n            <button type=\"submit\" class=\"btn btn-primary\">Save Category</button>\n            <button type=\"button\" class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#deleteModal\">Delete</button>\n        </template>\n    </form>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\staff\\admin.forum.boards.new.category.route.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"vue":33,"vue-hot-reload-api":6});

loadjs.d("./resources/assets/js/routes/staff/admin.forum.boards.route.vue",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    data: function data() {
        return {
            categories: []
        };
    },


    methods: {
        getBoards: function getBoards() {
            this.$http.get('forums/boards').then(function (response) {
                this.categories = response.data.forum_categories;
            }, function (response) {
                this.broadcastError(response);
            });
        },

        moveBoard: function moveBoard(id, direction, isChild) {
            var data = {
                direction: direction,
                isChild: isChild.toString()
            };
            this.$http.post('forums/board/' + id + '/reorder', data).then(function (response) {
                this.categories = response.data.forum_categories;
            }, function (response) {
                this.broadcastError(response);
            });
        },

        moveCategory: function moveCategory(id, direction) {
            var data = {
                direction: direction
            };
            this.$http.post('forums/category/' + id + '/reorder', data).then(function (response) {
                this.categories = response.data.forum_categories;
            }, function (response) {
                this.broadcastError(response);
            });
        }
    },

    route: {
        activate: function activate() {
            this.getBoards();
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div>\n    <a class=\"btn btn-lg btn-primary\" href=\"#\" role=\"button\" v-link=\"{path:'/staff/forum/categories/new'}\">\n        <span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> New Category\n    </a>\n\n    <hr>\n\n    <template v-for=\"category in categories\">\n        <div class=\"pull-right\">\n            <span class=\"small\">\n                Viewable by: <span class=\"glyphicon glyphicon-user\" style=\"color: red\"></span>\n            </span>\n        </div>\n        <h3><a href=\"\">{{category.name}}</a></h3>\n        <div class=\"well\">\n            <ul class=\"list-group\">\n\n                <template v-for=\"board in category.boards\">\n                <li class=\"list-group-item\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-6\">\n                            <strong>{{board.name}}</strong>\n                            <div class=\"small\">\n                                <span class=\"glyphicon glyphicon-user\" style=\"color: red\"></span>\n                            </div>\n                        </div>\n                        <div class=\"col-sm-6 text-right\">\n                            <div class=\"btn-group\" role=\"group\" aria-label=\"...\">\n                                <button type=\"button\" class=\"btn btn-default\" v-on:click=\"moveBoard(board.id, -1, false)\" v-bind:disabled=\"board.order == 1\">\n                                    <span class=\"glyphicon glyphicon-arrow-up\" aria-hidden=\"true\"></span>\n                                </button>\n                                <button type=\"button\" class=\"btn btn-default\" v-on:click=\"moveBoard(board.id, 1, false)\" v-bind:disabled=\"board.order == category.boards.length\">\n                                    <span class=\"glyphicon glyphicon-arrow-down\" aria-hidden=\"true\"></span>\n                                </button>\n                            </div>\n                            <button type=\"button\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> Edit</button>\n                        </div>\n                    </div>\n                    <ul class=\"list-group\" v-if=\"board.child_boards.length > 0\" style=\"margin-top:10px; margin-bottom:0\">\n                        <li class=\"list-group-item\" v-for=\"childboard in board.child_boards\">\n                            <div class=\"row\">\n                                <div class=\"col-sm-6\">\n                                    -- {{childboard.name}}\n                                    <div class=\"small\">\n                                        <span class=\"glyphicon glyphicon-user\" style=\"color: red\"></span>\n                                    </div>\n                                </div>\n                                <div class=\"col-sm-6 text-right\">\n                                    <div class=\"btn-group\" role=\"group\" aria-label=\"...\">\n                                        <button type=\"button\" class=\"btn btn-default\" v-on:click=\"moveBoard(childboard.id, -1, true)\" v-bind:disabled=\"childboard.order <= 1\">\n                                            <span class=\"glyphicon glyphicon-arrow-up\" aria-hidden=\"true\"></span>\n                                        </button>\n                                        <button type=\"button\" class=\"btn btn-default\" v-on:click=\"moveBoard(childboard.id, 1, true)\" v-bind:disabled=\"childboard.order >= board.child_boards.length\">\n                                            <span class=\"glyphicon glyphicon-arrow-down\" aria-hidden=\"true\"></span>\n                                        </button>\n                                    </div>\n                                    <button type=\"button\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> Edit</button>\n                                </div>\n                            </div>\n                        </li>\n                    </ul>\n                </li>\n                </template>\n\n                <hr>\n                <div class=\"btn-group pull-right\" role=\"group\" aria-label=\"...\">\n                    <button type=\"button\" class=\"btn btn-warning\" v-on:click=\"moveCategory(category.id, -1)\" v-bind:disabled=\"category.order <= 1\">\n                        <span class=\"glyphicon glyphicon-arrow-up\" aria-hidden=\"true\"></span>\n                    </button>\n                    <button type=\"button\" class=\"btn btn-warning\" v-on:click=\"moveCategory(category.id, 1)\" v-bind:disabled=\"category.order >= categories.length\">\n                        <span class=\"glyphicon glyphicon-arrow-down\" aria-hidden=\"true\"></span>\n                    </button>\n                </div>\n                <a class=\"btn btn-lg btn-warning\" href=\"#\" role=\"button\" v-link=\"{path:'/staff/forum/categories/edit/' + category.id}\">\n                    <span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> New Board\n                </a>\n                <a class=\"btn btn-lg btn-warning\" href=\"#\" role=\"button\" v-link=\"{path:'/staff/forum/categories/edit/' + category.id}\">\n                    <span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> Edit Category\n                </a>\n\n            </ul>\n        </div>\n    </template>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\staff\\admin.forum.boards.route.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"vue":33,"vue-hot-reload-api":6});

loadjs.d("./resources/assets/js/routes/staff/admin.forum.route.vue",function(require,module,exports){
var __vueify_style__ = require("vueify-insert-css").insert("\n.sidenav {\n    padding: 0 15px 0 0;\n    border-right: 1px solid #ccc;\n}\n\n")
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    data: function data() {
        return {};
    },


    methods: {},

    ready: function ready() {},

    route: {
        activate: function activate() {}
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div class=\"col-sm-12\">\n    <div class=\"panel panel-default\">\n        <div class=\"panel-body\">\n            <div class=\"col-sm-3 list-group sidenav\">\n                    <a href=\"#\" class=\"list-group-item active\" v-link=\"{path: '/staff/forum', activeClass: 'active'}\">\n                        <div class=\"pull-right\"><span class=\"glyphicon glyphicon-th-list\" aria-hidden=\"true\"></span></div>\n                        Boards &amp; Categories\n                    </a>\n                <a href=\"#\" class=\"list-group-item active\" v-link=\"{path: '/staff/forum/modules', activeClass: 'active'}\">\n                    <div class=\"pull-right\"><span class=\"glyphicon glyphicon-asterisk\" aria-hidden=\"true\"></span></div>\n                    Thread Modules\n                </a>\n            </div>\n            <div class=\"col-sm-9\">\n                <router-view transition=\"fade\" transition-mode=\"out-in\"></router-view>\n            </div>\n        </div>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\staff\\admin.forum.route.vue"
  module.hot.dispose(function () {
    require("vueify-insert-css").cache["\n.sidenav {\n    padding: 0 15px 0 0;\n    border-right: 1px solid #ccc;\n}\n\n"] = false
    document.head.removeChild(__vueify_style__)
  })
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"vue":33,"vue-hot-reload-api":6,"vueify-insert-css":34});

loadjs.d("./resources/assets/js/routes/staff/admin.index.route.vue",function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    data: function data() {
        return {};
    },


    methods: {},

    ready: function ready() {},

    route: {
        activate: function activate() {}
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div>\n    <div class=\"col-sm-12\">\n        <div class=\"panel panel-default\">\n            <div class=\"panel-body\">\n\n\n            </div>\n        </div>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\staff\\admin.index.route.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"vue":33,"vue-hot-reload-api":6});

loadjs.d("./resources/assets/js/routes/staff/admin.route.vue",function(require,module,exports){
var __vueify_style__ = require("vueify-insert-css").insert("\n.fade-transition\n{\n  -webkit-transition: opacity .1s ease;\n  transition: opacity .1s ease;\n}\n.fade-enter, .fade-leave\n{\n  opacity: 0;\n}\n")
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    data: function data() {
        return {};
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div>\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <div class=\"panel panel-default\">\n                <div class=\"panel-heading\">\n                    Website Control Panel\n                </div>\n                <div class=\"panel-body\">\n\n                    <ul class=\"nav nav-pills\">\n                        <li role=\"presentation\"><a href=\"\" v-link=\"{path: '/staff'}\">Main</a></li>\n                        <li role=\"presentation\"><a href=\"\" v-link=\"{path: '/staff/forum'}\">Forums</a></li>\n                        <li role=\"presentation\"><a href=\"\" v-link=\"{path: '/staff/servers'}\">Servers</a></li>\n                        <li role=\"presentation\"><a href=\"\" v-link=\"{path: ''}\">Users</a></li>\n                        <li role=\"presentation\"><a href=\"\" v-link=\"{path: ''}\">Roles &amp; Permissions</a></li>\n                        <li role=\"presentation\"><a href=\"\" v-link=\"{path: ''}\">Maintenance</a></li>\n                        <li role=\"presentation\"><a href=\"\" v-link=\"{path: ''}\">Settings</a></li>\n                    </ul>\n\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"row\">\n        <router-view transition=\"fade\" transition-mode=\"out-in\"></router-view>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\staff\\admin.route.vue"
  module.hot.dispose(function () {
    require("vueify-insert-css").cache["\n.fade-transition\n{\n  -webkit-transition: opacity .1s ease;\n  transition: opacity .1s ease;\n}\n.fade-enter, .fade-leave\n{\n  opacity: 0;\n}\n"] = false
    document.head.removeChild(__vueify_style__)
  })
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"vue":33,"vue-hot-reload-api":6,"vueify-insert-css":34});

loadjs.d("./resources/assets/js/routes/staff/admin.servers.list.route.vue",function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});


var moment = require('moment');

exports.default = {
    data: function data() {
        return {
            list: [],
            newServer: {
                name: '',
                ip: '',
                port: '',
                show_port: true,
                ip_alias: '',
                has_alias: false,
                category_type: 'existing',
                category: '',
                new_category: ''
            },
            deleteId: -1
        };
    },


    methods: {
        getServers: function getServers() {
            var self = this;
            this.$http.get('servers').then(function (response) {
                this.list = response.data;

                // check if each server has an ip alias
                for (var key in this.list) {

                    // also ensure a category is set on the New Server form
                    if (self.newServer.category == '') {
                        this.newServer.category = key;
                    }

                    var servers = this.list[key];
                    for (var x = 0; x < servers.length; x++) {
                        var server = servers[x];
                        if (server.ip_alias != null) {
                            server.has_alias = true;
                        }
                    }
                }
            }, function (response) {
                console.log(response);
            });
        },

        getLastStatus: function getLastStatus(server) {
            if (server.latest_status == null) return "Never";

            return moment(server.latest_status.date).fromNow();
        },

        setTag: function setTag(server, tag) {
            var data = {
                tag: tag
            };
            this.$http.post('servers/' + server.id + '/tag', data).then(function (response) {
                if (response.data.success) {
                    server.tag = tag;
                    this.$dispatch('server-ping');
                }
            }, function (response) {
                console.log(response);
            });
        },

        saveServer: function saveServer(server) {
            var data = {
                name: server.name,
                ip: server.ip,
                port: server.port,
                show_port: server.show_port
            };
            if (server.has_alias) {
                data.ip_alias = server.ip_alias;
            }
            if (server.category_type == 'new') {
                data.type = server.new_category;
            } else {
                data.type = server.category;
            }
            this.$http.post('servers/' + server.id + '/save', data).then(function (response) {
                if (response.data.success) {
                    $('#server_' + server.id).collapse('hide');
                    this.getServers();

                    this.$dispatch('server-ping');
                }
            }, function (response) {
                console.log(response);
            });
        },

        setModalServer: function setModalServer(id) {
            this.deleteId = id;
        },

        deleteServer: function deleteServer() {
            var id = this.deleteId;
            if (id == -1) return;

            this.$http.post('servers/' + id + '/delete', {}).then(function (response) {
                if (response.data.success) {
                    $('#deleteModal').modal('hide');
                    this.getServers();

                    this.$dispatch('server-ping');
                }
            }, function (response) {
                console.log(response);
            });
        },

        createServer: function createServer() {
            var server = this.newServer;
            var data = {
                name: server.name,
                ip: server.ip,
                port: server.port,
                show_port: server.show_port
            };
            console.log(server.show_port);
            if (server.has_alias) {
                data.ip_alias = server.ip_alias;
            }
            if (server.category_type == 'new') {
                data.type = server.new_category;
            } else {
                data.type = server.category;
            }
            this.$http.post('servers/create', data).then(function (response) {
                if (response.data.success) {
                    $('#new_server_form').collapse('hide');
                    this.getServers();

                    this.newServer = {
                        name: '',
                        ip: '',
                        port: '',
                        show_port: true,
                        ip_alias: '',
                        has_alias: false,
                        category_type: 'existing',
                        category: '',
                        new_category: ''
                    };

                    this.$dispatch('server-ping');
                }
            }, function (response) {
                console.log(response);
            });
        }
    },

    route: {
        activate: function activate() {
            this.getServers();
        }
    }
};
if (module.exports.__esModule) module.exports = module.exports.default
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div>\n    <div class=\"modal fade\" id=\"deleteModal\" tabindex=\"-1\" role=\"dialog\">\n        <div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span></button>\n                    <h4 class=\"modal-title\">Delete this server?</h4>\n                </div>\n                <div class=\"modal-body\">\n                    <p>This cannot be undone. Status records for this server will not be deleted.</p>\n                </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-danger\" v-on:click=\"deleteServer\">Delete Server</button>\n                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <a class=\"btn btn-primary\" role=\"button\" data-toggle=\"collapse\" href=\"#new_server_form\" aria-expanded=\"false\" aria-controls=\"collapseExample\">\n        <span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> New Server\n    </a>\n\n    <div class=\"row collapse\" id=\"new_server_form\">\n        <hr>\n\n        <div class=\"well\">\n            <form class=\"form-horizontal\" v-on:submit.prevent=\"createServer\">\n                <div class=\"form-group\">\n                    <label class=\"col-sm-2 control-label\">Name</label>\n                    <div class=\"col-sm-10\">\n                        <input type=\"text\" class=\"form-control\" v-model=\"newServer.name\" placeholder=\"eg. Pixelmon\">\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-sm-2 control-label\">Category</label>\n                    <div class=\"col-sm-10\">\n                        <div class=\"input-group\">\n                            <span class=\"input-group-addon\">\n                              <input type=\"radio\" aria-label=\"...\" value=\"existing\" v-model=\"newServer.category_type\" checked=\"\">\n                            </span>\n                            <select class=\"form-control\" v-model=\"newServer.category\" v-bind:disabled=\"newServer.category_type == 'new'\">\n                                <template v-for=\"category in list\">\n                                    <template v-if=\"$index == 0\">\n                                        <option v-bind:value=\"$key\" selected=\"\">{{$key | capitalize}}</option>\n                                    </template>\n                                    <template v-else=\"\">\n                                        <option v-bind:value=\"$key\">{{$key | capitalize}}</option>\n                                    </template>\n                                </template>\n                            </select>\n                        </div>\n                        <div class=\"input-group\">\n                            <span class=\"input-group-addon\">\n                              <input type=\"radio\" aria-label=\"...\" value=\"new\" v-model=\"newServer.category_type\">\n                            </span>\n                            <input type=\"text\" class=\"form-control\" aria-label=\"...\" placeholder=\"New Category Name\" v-model=\"newServer.new_category\" v-bind:disabled=\"newServer.category_type == 'existing'\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-sm-2 control-label\">IP</label>\n                    <div class=\"col-sm-10\">\n                        <input type=\"text\" class=\"form-control\" v-model=\"newServer.ip\" placeholder=\"eg. 198.144.156.53\">\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-sm-2 control-label\">Port</label>\n                    <div class=\"col-sm-10\">\n                        <input type=\"text\" class=\"form-control\" v-model=\"newServer.port\" placeholder=\"eg. 25565\">\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-sm-2 control-label\">IP Alias</label>\n                    <div class=\"col-sm-10\">\n                        <div class=\"input-group\">\n                            <span class=\"input-group-addon\">\n                                <input type=\"checkbox\" aria-label=\"...\" v-model=\"newServer.has_alias\">\n                            </span>\n                            <input type=\"text\" class=\"form-control\" v-model=\"newServer.ip_alias\" aria-label=\"...\" v-bind:disabled=\"!newServer.has_alias\" placeholder=\"eg. pcbmc.co\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <div class=\"col-sm-offset-2 col-sm-10\">\n                        <div class=\"checkbox\">\n                            <label>\n                                <input type=\"checkbox\" v-model=\"newServer.show_port\"> Show Port\n                            </label>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <div class=\"col-sm-offset-2 col-sm-10\">\n                        <button type=\"submit\" class=\"btn btn-primary\">Add Server</button>\n                    </div>\n                </div>\n            </form>\n        </div>\n    </div>\n\n\n    <hr>\n\n    <template v-for=\"game in list\">\n        <h3>{{$key | capitalize}}</h3>\n        <div class=\"well\">\n            <ul class=\"list-group\">\n\n                <template v-for=\"server in game\">\n                <li class=\"list-group-item\" v-bind:class=\"{\n                    'list-group-item-warning': server.tag == 'maintenance',\n                    'list-group-item-danger': server.tag == 'offline',\n                    'list-group-item-info': server.tag == 'hidden',\n                }\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-8\">\n                            <h4>\n                                {{server.name}}\n                                <span class=\"small\">( {{server.ip}} | {{server.port}} )</span>\n                            </h4>\n                        </div>\n                        <div class=\"col-sm-4 text-right\">\n                            Last Queried: {{getLastStatus(server)}}\n                        </div>\n                    </div>\n\n                    <div class=\"row collapse\" id=\"server_{{server.id}}\">\n                        <div class=\"well\">\n                            <form class=\"form-horizontal\" v-on:submit.prevent=\"saveServer(server)\">\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Name</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" class=\"form-control\" v-model=\"server.name\" placeholder=\"eg. Pixelmon\">\n                                    </div>\n                                </div>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Category</label>\n                                    <div class=\"col-sm-10\">\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                              <input type=\"radio\" aria-label=\"...\" value=\"existing\" v-model=\"server.category_type\" checked=\"\">\n                                            </span>\n                                            <select class=\"form-control\" v-model=\"server.category\" v-bind:disabled=\"server.category_type == 'new'\">\n                                                <template v-for=\"category in list\">\n                                                    <template v-if=\"server.type == $key\">\n                                                        <option v-bind:value=\"$key\" selected=\"\">{{$key | capitalize}}</option>\n                                                    </template>\n                                                    <template v-else=\"\">\n                                                        <option v-bind:value=\"$key\">{{$key | capitalize}}</option>\n                                                    </template>\n                                                </template>\n                                            </select>\n                                        </div>\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                              <input type=\"radio\" aria-label=\"...\" value=\"new\" v-model=\"server.category_type\">\n                                            </span>\n                                            <input type=\"text\" class=\"form-control\" aria-label=\"...\" placeholder=\"New Category Name\" v-model=\"server.new_category\" v-bind:disabled=\"server.category_type == 'existing'\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">IP</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" class=\"form-control\" v-model=\"server.ip\" placeholder=\"eg. 198.144.156.53\">\n                                    </div>\n                                </div>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">Port</label>\n                                    <div class=\"col-sm-10\">\n                                        <input type=\"text\" class=\"form-control\" v-model=\"server.port\" placeholder=\"eg. 25565\">\n                                    </div>\n                                </div>\n                                <div class=\"form-group\">\n                                    <label class=\"col-sm-2 control-label\">IP Alias</label>\n                                    <div class=\"col-sm-10\">\n                                        <div class=\"input-group\">\n                                            <span class=\"input-group-addon\">\n                                                <input type=\"checkbox\" aria-label=\"...\" v-model=\"server.has_alias\">\n                                            </span>\n                                            <input type=\"text\" class=\"form-control\" v-model=\"server.ip_alias\" aria-label=\"...\" v-bind:disabled=\"!server.has_alias\" placeholder=\"eg. pcbmc.co\">\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"form-group\">\n                                    <div class=\"col-sm-offset-2 col-sm-10\">\n                                        <div class=\"checkbox\">\n                                            <label>\n                                                <input type=\"checkbox\" v-model=\"server.show_port\"> Show Port\n                                            </label>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"form-group\">\n                                    <div class=\"col-sm-offset-2 col-sm-10\">\n                                        <button type=\"submit\" class=\"btn btn-primary\">Save Server</button>\n                                        <button type=\"button\" class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#deleteModal\" v-on:click=\"setModalServer(server.id)\">Delete</button>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"col-sm-12\">\n                            <div class=\"btn-group pull-right\" role=\"group\" aria-label=\"...\">\n                                <button type=\"button\" class=\"btn btn-default\">\n                                    <span class=\"glyphicon glyphicon-arrow-up\" aria-hidden=\"true\"></span>\n                                </button>\n                                <button type=\"button\" class=\"btn btn-default\">\n                                    <span class=\"glyphicon glyphicon-arrow-down\" aria-hidden=\"true\"></span>\n                                </button>\n                            </div>\n                            <a class=\"btn btn-default\" role=\"button\" data-toggle=\"collapse\" href=\"#server_{{server.id}}\" aria-expanded=\"false\" aria-controls=\"collapseExample\">\n                                <span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> Edit\n                            </a>\n                            <button type=\"button\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-refresh\" aria-hidden=\"true\"></span> Ping</button>\n                            <div class=\"btn-group\">\n                                <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                                    <span class=\"glyphicon glyphicon-tag\" aria-hidden=\"true\"></span> {{server.tag | capitalize}} <span class=\"caret\"></span>\n                                </button>\n                                <ul class=\"dropdown-menu\">\n                                    <li><a href=\"#\" v-on:click.prevent=\"setTag(server, 'normal')\">Normal / No Tag</a></li>\n                                    <li><a href=\"#\" v-on:click.prevent=\"setTag(server, 'offline')\">Offline</a></li>\n                                    <li><a href=\"#\" v-on:click.prevent=\"setTag(server, 'maintenance')\">Maintenance</a></li>\n                                    <li role=\"separator\" class=\"divider\"></li>\n                                    <li><a href=\"#\" v-on:click.prevent=\"setTag(server, 'hidden')\">Hidden</a></li>\n                                </ul>\n                            </div>\n                        </div>\n                    </div>\n                </li>\n                </template>\n            </ul>\n        </div>\n    </template>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\staff\\admin.servers.list.route.vue"
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"moment":4,"vue":33,"vue-hot-reload-api":6});

loadjs.d("./resources/assets/js/routes/staff/admin.servers.route.vue",function(require,module,exports){
var __vueify_style__ = require("vueify-insert-css").insert("\n.sidenav {\n    padding: 0 15px 0 0;\n    border-right: 1px solid #ccc;\n}\n")
;(typeof module.exports === "function"? module.exports.options: module.exports).template = "\n<div class=\"col-sm-12\">\n    <div class=\"panel panel-default\">\n        <div class=\"panel-body\">\n            <div class=\"col-sm-3 list-group sidenav\">\n                <a href=\"#\" class=\"list-group-item active\" v-link=\"{path: '/staff/servers', activeClass: 'active'}\">\n                    <div class=\"pull-right\"><span class=\"glyphicon glyphicon-th-list\" aria-hidden=\"true\"></span></div>\n                    Server List\n                </a>\n                <a href=\"#\" class=\"list-group-item active\" v-link=\"{path: '/staff/servers/statuses', activeClass: 'active'}\">\n                    <div class=\"pull-right\"><span class=\"glyphicon glyphicon-asterisk\" aria-hidden=\"true\"></span></div>\n                    Statuses\n                </a>\n            </div>\n            <div class=\"col-sm-9\">\n                <router-view transition=\"fade\" transition-mode=\"out-in\"></router-view>\n            </div>\n        </div>\n    </div>\n</div>\n"
if (module.hot) {(function () {  module.hot.accept()
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), true)
  if (!hotAPI.compatible) return
  var id = "E:\\xampp\\htdocs\\PCB_update\\resources\\assets\\js\\routes\\staff\\admin.servers.route.vue"
  module.hot.dispose(function () {
    require("vueify-insert-css").cache["\n.sidenav {\n    padding: 0 15px 0 0;\n    border-right: 1px solid #ccc;\n}\n"] = false
    document.head.removeChild(__vueify_style__)
  })
  if (!module.hot.data) {
    hotAPI.createRecord(id, module.exports)
  } else {
    hotAPI.update(id, module.exports, (typeof module.exports === "function" ? module.exports.options : module.exports).template)
  }
})()}
},{"vue":33,"vue-hot-reload-api":6,"vueify-insert-css":34});
